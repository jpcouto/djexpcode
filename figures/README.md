# Figures

## Figure 1

#### Panel B - Standard deviation of widefield triggered by visual and airpuff stimuli

This is from JC092

_Notebook:_ notebooks/paper-figures/figure1/example_visual_tactile_widefield_JC092.ipynb


![picture](figure1/wfield_examples/JC092_example_wfield_visual_tactile_190124_JC092_1P_JC.png)

#### Panel D - two photon example multiple planes examples

_Notebook:_ notebooks/paper-figures/figure1/example_allen_matched_2p_JC048.ipynb

There are 1342/11858 plotted cells this figure from 6 sessions, 1 mouse.

![picture](figure1/2p_examples/fig1_allen_matched_fov_JC048.png)


#### Panel E - two photon df/f rasters for the 1000 most responsive cells

     Has responses for visual stimuli (top) and airpuffs (bottom) for S1 (left) and V1 (right).

     Nmice = 
     Ncells =

![picture](figure1/2p_examples/df_f_normalized_examples_visual_puff.png)

# Panel F - Cumulative histograms of puff amplitudes

_Notebook:_ notebooks/paper-figures/figure1/figure1-summaries.ipynb

KS-tests for S1:

* 171102_JC048_2P_JC 	** p val: 1.7043231278845774e-79
* 171107_JC048_2P_S1_JC 	** p val: 8.414844315369018e-08
* 181008_JC079_2P_JC 	** p val: 5.964214221178433e-10
* 190126_JC092_2P_S1_JC 	 not sig p val: 0.10225641683171434

KS-tests for V1:

* 180118_EV038_2P_JC 	** p val: 3.6630076826792698e-40
* 171102_JC048_2P_JC 	 not sig p val: 0.42188157096615353
* 171107_JC048_2P_V1_JC 	** p val: 5.407452355905845e-09
* 180425_JC054_2P_JC 	 not sig p val: 0.932862881855788
* 180425_JC059_2P_JC 	 not sig p val: 0.06016991547123758
* 180515_JC062_2P_JC 	 not sig p val: 0.7027893197060042
* 181009_JC079_2P_JC 	 not sig p val: 0.12084007990236585
* 190204_JC092_2P_V1_JC 	 not sig p val: 0.7034413840304077
* 180209_SK072_2P_JC 	 not sig p val: 0.6763238172825639

![picture](figure1/mean_puff_stim_cumulative_compare.png)

Figure with only the cells with amplitudes above the 75th percentile

![picture](figure1/mean_puff_stim_cumulative_compare_75perc.png)

Figure with the amplitudes pre-stim
_Important todo_ check df/f values for the experiments that are different.

![picture](figure1/mean_puff_stim_all_sighted_one_session_shifted.png)

Figure with the fraction of responsive trials cell

![picture](figure1/fraction_puff_responsive_all_sighted_one_session.png)

Figure with the cumulative histograms of amplitude and fraction of responsive trials 

![picture](figure1/mean_puff_amplitudes.png)

Figure for the amplitude versus the fraction of responsive trials

![picture](figure1/amplitude_fraction_responsive_trials.png)

Figure for the fraction of responsive trials per experiment (15% of the trials)

![picture](figure1/cells_responding_15perc_trials.png)


## Figure 3

#### Panel A - Diff of std of mapmaps

_Notebook:_ notebooks/data-import/reference_reg/references_manual_imports_JC092_widefield_cues.ipynb

![picture](figure3/lapmaps_light_dark_diff_JC092.png)

