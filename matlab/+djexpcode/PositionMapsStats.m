%{
# 
-> djexpcode.PositionMaps
---
n_active_cells=0            : smallint                      # 
n_position_cells=0          : smallint                      # 
n_total_cells=0             : smallint                      # 
mean_velocity_map           : longblob                      # 
mean_cue_velocity_map=null  : longblob                      # 
n_trials=0                  : smallint                      # 
%}

classdef PositionMapsStats < dj.Computed
    methods(Access=protected)
        function makeTuples(self,key)
        end
    end
end
