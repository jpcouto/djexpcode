%{
# 
-> djexpcode.SessionRun
---
-> djexpcode.TreadmillBelt
enucleated=0                : smallint                      # whether the mouse was enucleated
headpost_angle              : float                         # deg
shield_distance             : float                         # cm
led_eye                     : enum('on','off','variable')   # LED on or off
light_conditions            : enum('light','dark','vstim')  # light conditions of the experiment
whiskers                    : enum('intact','cut','contra_cut','ipsi_cut') # whisker cutting
whisker_block               : smallint                      # has blocker under the whiskers
eye_blocker                 : smallint                      # has eye blocker
notes                       : varchar(4000)                 # free-text notes
%}

classdef TreadmillPositionExperiment < dj.Manual
end
