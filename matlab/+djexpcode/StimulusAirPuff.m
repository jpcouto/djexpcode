%{
# Air puff stimuli
-> djexpcode.TwoPhotonVolume
---
valve_pulses                : longblob                      # 
stim_onsets                 : longblob                      # (s)
stim_offsets                : longblob                      # (s)
intra_pulse_frequency       : longblob                      # (Hz)
puff_durations              : longblob                      # (s)
puff_duration               : float                         # (s)
n_trials                    : smallint                      # Number of trials
%}

classdef StimulusAirPuff < dj.Computed
    methods(Access=protected)
        function makeTuples(self,key)
        end
    end
end
