% +tp/getSchema.m - link package 'tp' to database 'two-photon'
function obj = getSchema
persistent SCHEMA
if isempty(SCHEMA)
    SCHEMA = dj.Schema(dj.conn, 'djexpcode', 'experiments_joao');
end
obj = SCHEMA;
end