%{
# Regions of interest for neurons
-> djexpcode.Segmentation
-> djexpcode.Segmentation
roi                         : smallint                      # region of interest within image plane
---
roi_pixels                  : longblob                      # pixel indices
neuropil_pixels             : longblob                      # pixel indices
%}
classdef SegmentationROI < dj.Part
    properties(SetAccess=protected)
        master = djexpcode.Segmentation
    end
    methods
        function makeTuples(self, key)
        end
     end
end
