%{
# 
-> djexpcode.PositionMaps
-> djexpcode.SegmentationTraces
---
large_events_per_lap        : blob                          # 
df_f_map                    : longblob                      # 
deconv_map                  : longblob                      # 
cue_maps=null               : longblob                      # 
%}
classdef PositionMapsActivityMaps < dj.Part
    properties(SetAccess=protected)
        master = djexpcode.PositionMaps
    end
    methods
        function makeTuples(self, key)
        end
     end
end
