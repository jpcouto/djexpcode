%{
# 
-> djexpcode.PositionMaps
-> djexpcode.SegmentationROI
---
large_events_per_lap        : blob                          # 
df_f_map                    : longblob                      # 
deconv_map                  : longblob                      # 
%}
classdef PositionMapsActivityMaps < dj.Part
    properties(SetAccess=protected)
        master = djexpcode.PositionMaps
    end
    methods
        function makeTuples(self, key)
        end
     end
end
