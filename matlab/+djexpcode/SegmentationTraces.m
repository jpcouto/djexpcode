%{
# Calcium transients
-> djexpcode.SegmentationROI
---
df_f                        : longblob                      # df/f
neuropil                    : longblob                      # neuropil
denoised                    : longblob                      # denoised df/f
deconv                      : longblob                      # infered rate
baseline                    : float                         # baseline
std                         : float                         # std of df/f
deconvolution_parameters    : blob                          # parameters of the deconvolution
%}
classdef SegmentationTraces < dj.Part
    properties(SetAccess=protected)
        master = djexpcode.Segmentation
    end
    methods
        function makeTuples(self, key)
        end
     end
end
