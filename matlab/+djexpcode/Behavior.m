%{
# Behavior during the session run
-> djexpcode.SessionRun
---
behaviortime=null           : longblob                      # timebase
behaviorposition=null       : longblob                      # position of the animal on the track
behaviorvelocity=null       : longblob                      # velocity of the animal
laptimes=null               : longblob                      # times of lap events
licktimes=null              : longblob                      # times of lick events
%}

classdef Behavior < dj.Imported
     methods(Access=protected)
        
        function makeTuples(self, key)
        end
     end
end
