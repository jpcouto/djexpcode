%{
# 
-> djexpcode.PositionMapsStats
-> djexpcode.PositionMapsActivityMaps
---
position_ev=0               : float                         # 
position_reliability=0      : float                         # 
position_var_mean=0         : float                         # 
position_fraction_responsive_trials=0: float                # 
is_position=0               : smallint                      # 
cue_amplitude=null          : float                         # 
cue_amplitudes=null         : blob                          # 
mean_cue_df_f=null          : longblob                      # 
mean_position_df_f          : longblob                      # 
mean_position_deconv        : longblob                      # 
%}
classdef PositionMapsStatsActivityMapStats < dj.Part
    properties(SetAccess=protected)
        master = djexpcode.PositionMapsStats
    end
    methods
        function makeTuples(self, key)
        end
     end
end


%%

% dd = fetch(djexpcode.PositionMapsStatsActivityMapStats & 'mouse_id = "EV038"' & 'run_num = 0','mean_position_df_f','position_ev')
% [~,idx] = sort([dd.position_ev]);
% imagesc([dd(idx).mean_position_df_f]',[0,75])
