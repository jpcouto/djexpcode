djexpcode
=========

Datajoint framework for managing two-photon and electrophysiology experiments used in the multimodal V1 project. 

Organization
-----------------

All code is in the djexpcode module, the schemas are in djexpcode.schemas

Notebooks:

- notebooks                           General or misplaced notebooks
- notebooks/data-imports              Used to import data into the database
- notebooks/paper-figures             Used to generate the paper figures
- notebooks/project                   Used to browse data; debug code or explore data


Datajoint schemas
-----------------

Data analysis was done with datajoint.

![picture](https://bitbucket.org/jpcouto/djexpcode/raw/tacnav/figures/schemasdiagram.png)

