try:
    from pyvstim import parseVStimLog,treadmillBehaviorFromRelativePosition
except:
    print('Could not load pyvstim')
import numpy as np
from scipy.interpolate import interp1d 
from scipy.stats import mode

__all__ = ['treadmillBehaviorFromRelativePosition',
           'getLinearTreadmillBehaviorFromLog',
           'binToLaps','interpolateLapMap']

def _treadmillBehaviorFromRelativePosition(time,pos,
                                          srate = 90.):
    ''' Relative position to belt behavior normalized 
        (needs to be multiplied by the belt length)'''

    lapidx = np.where(np.diff(pos) < -1000)[0]

    if len(lapidx) < 1:
        # this is not correct but handles lack of behavior
        position = pos/pos.max()
        displacement = pos/pos.max()
        laptimes = []
    else:
        laptimes = time[lapidx]

        displacement = np.zeros_like(pos,dtype=np.float32)
        position = np.zeros_like(pos,dtype=np.float32)
        meanlapticks = int(mode(pos[lapidx]).mode)

        # Fix the first lap
        idx = np.arange(0,lapidx[0]+1)
        position[idx] = pos[idx]/pos[idx[-1]]
        displacement[idx] = pos[idx]/pos[idx[-1]]
        lapcounter = 1.
        for ii,lapstart in enumerate(lapidx[:-1]):
            idx = np.arange(lapstart+1, lapidx[ii+1]+1)
            position[idx] = pos[idx]/pos[idx[-1]]
            displacement[idx] = pos[idx]/pos[idx[-1]] + lapcounter
            lapcounter += 1
        # Fix the last lap
        idx = np.arange(lapidx[-1]+1,len(pos))
        position[idx] = pos[idx]/meanlapticks
        displacement[idx] = pos[idx]/meanlapticks + lapcounter
    # Interpolate to get a uniform sampling rate
    behaviortime = np.arange(np.min(time),np.max(time),1./srate)
    position = interp1d(time,position,
                        kind = 'nearest',
                        copy=False,
                        bounds_error=False,
                        fill_value=np.nan)(behaviortime)
    displacement = interp1d(time,displacement,
                            kind = 'linear',
                            copy=False,
                            bounds_error=False,
                            fill_value='extrapolate')(behaviortime)
    velocity = np.diff(np.hstack([displacement[0],displacement]))/(1./srate)
    return behaviortime,position,displacement,velocity,laptimes

def getLinearTreadmillBehaviorFromLog(filename,
                                      relativeLap=True):
    logdata,comments = parseVStimLog(filename)
    if not(len(logdata.keys())):
        return None
    assert relativeLap, "Not implemented."
    # Deal with no behavior
    try:
        nlaps = len(logdata['lap']['duinotime'])
    except KeyError:
        nlaps = np.array([])
    if nlaps<4:
        return dict(behaviortime = [],
                    behaviorposition = [],
                    behaviorvelocity = [],
                    laptimes = [],
                    licktimes = [])
    if 'position' in logdata.keys():
        if not 'duinotime' in logdata['position'].columns:
            return dict(behaviortime = [],
                        behaviorposition = [],
                        behaviorvelocity = [],
                        laptimes = [],
                        licktimes = [])


    (behaviortime,
     position,
     displacement,
     velocity,
     laptimes) = treadmillBehaviorFromRelativePosition(np.array(logdata['position']['duinotime'])/1000.,
                                                       np.array(logdata['position']['value']))
    try:
        licktimes = np.array(logdata['lick']['duinotime'])/1000.
    except KeyError:
        licktimes = np.array([])
    return dict(behaviortime = behaviortime,
                behaviorposition = position,
                behaviorvelocity = velocity,
                laptimes = laptimes,
                licktimes = licktimes)

def binToLaps(laps,time,position,X,lapspace = None,
              velocity = None,
              velocityThreshold = 1., beltLength=150.,
              method=lambda x : np.nanmean(x,axis=1)):
    '''
    Bins vector X to lap position.
        - laps is [numberOfLaps,2] the start and stop time of each lap
    
    '''
    if lapspace is None:
        lapspace = np.arange(0,beltLength,1.)
    lapX = np.zeros((X.shape[0],laps.shape[0],lapspace.shape[0]-1))
    lapX[:] = np.nan
    for k,l in enumerate(range(laps.shape[0])):
        s,e = laps[l,:]
        
        if not velocity is None:
            idx = np.where((time>=s) & (time<e) &
                          (velocity > velocityThreshold))[0].astype(int)
        else:
            idx = np.where((time>=s) & (time<e))[0].astype(int)
        x = X[:,idx]
        pos = position[idx]
        inds = np.digitize(pos, lapspace)
        for i in np.unique(inds)[:-1]:
            lapX[:,k,i-1] = method(x[:,(inds==i)])
    return lapX

def interpolateLapMap(X):
    nn = np.arange(X.shape[2])
    from scipy.interpolate import interp1d
    nX = X.copy()
    for i in range(X.shape[1]):
        mask = ~np.isnan(X[0,i,:])
        nX[:,i,:] = interp1d(nn[mask],X[:,i,mask],axis = 1,fill_value ='extrapolate')(nn)
    return nX
