from glob import glob
import os
import sys
import json
from os.path import join as pjoin

preferencepath = pjoin(os.path.expanduser('~'), 'djexpcode')

defaultPreferences = {'datajoint':{'database.host':'localhost',
                                   'database.user':'root',
                                   'database.password':'Gcamp99',
                                   'database.name':'experiments_joao',
                                   'safemode':False,
                                   'reserve_jobs':True,
                                   'display.limit':30},
                      'datapaths':dict(dataserverpaths = ['/quadraraid/data',
                                                          '/mnt/nerffs01/mouselab/data'],
                                       onephotonpaths = '1photon/raw',
                                       twophotonpaths = dict(raw='2photon/raw',
                                                             reg='2photon/reg',
                                                             regds='2photon/reg_ds',
                                                             projections = '2photon/reg_ds'),
                                       logpaths = 'presentation',
                                       facecampaths = 'facecam',
                                       eyecampaths = 'eyecam',
                                       analysis = 'analysis'),
                      'backup':dict(server = '/mnt/nerfhf01/boninwip/data',
                                    folders = ['log','facecam','eyecam','twophoton:raw'])}

all = ['getPreferences','preferences','readLogComments','getImagingFrameTimes',
       'getROIsFromSuite2p','getROIsFromCaimanSelected','getROIsFromMaskNeuronsMat',
       'csc_matSavez','csc_matToDict','csc_matFromDict',
       'countEventsAboveThreshold','lowpass',
       'runpar']

def getPreferences():
    ''' Reads the user parameters from the home directory.

    pref = read_user_preferences(expname)

    User parameters like folder location, file preferences, paths...
    Joao Couto - Feb 2018
    '''
    if not os.path.isdir(preferencepath):
        os.makedirs(preferencepath)
        print('Creating .preference folder ['+preferencepath+']')

    preffile = pjoin(preferencepath,'preferences.json')
    if not os.path.isfile(preffile):
        with open(preffile, 'w') as outfile:
            json.dump(defaultPreferences, outfile, sort_keys = True, indent = 4)
            print('Saving default preferences to: ' + preffile)
    with open(preffile, 'r') as infile:
        pref = json.load(infile)
    return pref


preferences = getPreferences()


def structuredArrayToDict(entries):
    out = {}
    for k,key in enumerate(entries.dtype.names):
        entry = entries[key] 
        if not type(entry) is str:
            try:
                if entry.dtype.char == 'S':
                    entry = entry.astype(str)
            except:
                pass
        out[key] = entry
    return out


def removeNaN(data):
    
    ''' 
    datainterpolated = removeNaN(data)
    Remove NaN by linear interpolation.
    '''
    mask = ~np.isfinite(data)
    data[mask] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), data[~mask]) 
    return data


def readLogComments(fname):
    comments = []
    with open(fname,'r') as fd:
        for line in fd:
            if line.startswith('#'):
                comments.append(line.strip('\n').strip('\r'))
    return comments

def readScanBoxInfo(sbxfile):
    from scipy.io import loadmat

    matfile = os.path.splitext(sbxfile)[0] + '.mat'
    if not os.path.exists(matfile):
        raise('Metadata not found: {0}'.format(matfile))
    info = loadmat(matfile,squeeze_me = True,struct_as_record=False)['info']
    if info.channels == 1:
        nchan = 2; factor = 1
    elif info.channels == 2:
        nchan = 1
        factor = 2
    elif info.channels == 3:
        nchan = 1
        factor = 2
    optowavedepths = info.otwave_um
    try:
        depth = info.config.knobby.pos.z
        objective_angle = info.config.knobby.pos.a
    except:
        depth = info.config.coord_rel[2]
        objective_angle = info.config.coord_abs[-1]
    try:
        objective = info.objective
    except:
        objective = 'Unknown'
    if info.volscan:
        depths = info.otwave_um
        nplanes = len(depths)
    else:
        depths = 0
        nplanes = 1
    if info.scanmode == 0:
        #bidirectional
        frame_rate = 2./(info.config.lines/info.resfreq)
    else:
        frame_rate = 1./(info.config.lines/info.resfreq)
    ninfo = dict(nchannels = nchan,
                 nlines = info.config.lines,
                 ncols = info.sz[1],
                 pmtgain = [info.config.pmt0_gain,info.config.pmt1_gain],
                 laser_wavelength = info.config.wavelength,
                 objective_angle =  objective_angle,
                 depths = depths + depth,
                 objective = objective,
                 nplanes = nplanes,
                 frame_rate = frame_rate,
                 frames = info.config.frames,
                 magnification = float(info.config.magnification_list[
                     info.config.magnification-1]))
    return ninfo

from tifffile import imread
from glob import glob
import numpy as np
from .pathutils import searchDataServers

def getImagingFrametimes(session_name,session_subname,nplanes,plane,channel):
    '''Gets the frametimes of an experiment from reg data and the imaging log'''
    planeFilename = 'plane{plane:03}_ch{channel:02}'
    
    regpath = pjoin(preferences['datapaths']['twophotonpaths']['reg'],
                    '{0}'.format(session_name),
                    '{0}'.format(session_subname),
                    planeFilename.format(plane=plane+1,
                                         channel=channel))
    regfiles = searchDataServers(regpath,'tif')
    if not len(regfiles) and nplanes == 1:
        # Could be single plane data.
        print('Trying single plane format.')
        regpath = pjoin(preferences['datapaths']['twophotonpaths']['reg'],
                        '{0}'.format(session_name),
                        '{0}'.format(session_subname))
        regfiles = searchDataServers(regpath,'tif')
        
    
    durFirst = imread(regfiles[0]).shape[0]
    durLast = imread(regfiles[-1]).shape[0]
    nregframes = nplanes*((durFirst*(len(regfiles) - 1) + durLast))

    logname = "{0}.log".format(session_subname)
    logpath = pjoin(preferences['datapaths']['logpaths'],"{0}".format(session_name))
    logfile = searchDataServers(logpath,logname,'')
    if len(logfile):
        logfile = logfile[0]
    else:
        raise(OSError('File not found [{0} in {1}].'.format(logname,logpath)))
    from pyvstim import parseVStimLog
    log,comments = parseVStimLog(logfile)
    if not len(log.keys()):
        print('Using presentation log file parser.')
        from djexpcode.presentationlog import parsePresentationLog
        log = parsePresentationLog(logfile)
        frametimes = log['frametimes']
        frametimes = interp1d(np.arange(len(frametimes)),frametimes)(np.arange(0,nregframes,1))
        print(len(frametimes),nregframes)
    else:
        tstart = float(log['imaging'].iloc[0]['duinotime'])/1000.
        firstFrameTime = float(log['imaging']['duinotime'].iloc[0])/1000.
        lastFrameTime = float(log['imaging']['duinotime'].iloc[
            np.where(log['imaging']['value'] <= nregframes)[0][-1]])/1000
        frametimes = interp1d(log['imaging']['value'],
                              log['imaging']['duinotime']/1000.,
                              kind='linear', fill_value='extrapolate')(np.arange(1,nregframes+1,1))
    return frametimes

##################################################
############## Segmentation IO ###################
##################################################
def getROIsFromSuite2p(session_name,session_subname,plane,channel=0,**kwargs):

    planeFilename = 'plane{plane:03}_ch{channel:02}'

    regpath = pjoin(preferences['datapaths']['twophotonpaths']['reg'],
                    '{0}'.format(session_name),
                    '{0}'.format(session_subname),
                    planeFilename.format(plane=plane+1,
                                         channel=channel))

    s2ppath = searchDataServers(regpath,'','suite2p')
    if len(s2ppath):
        if len(s2ppath):
            F = np.load(pjoin(s2ppath[0],'plane0','F.npy'))
            Fneu = np.load(pjoin(s2ppath[0],'plane0','Fneu.npy'))
            correctedF = F-0.7*Fneu
            F0 = np.median(correctedF,axis=1)
            df_f = (100*((correctedF).T -F0)/F0).T
            spks = np.load(pjoin(s2ppath[0],'plane0','spks.npy'))
            iscell = np.load(pjoin(s2ppath[0],'plane0','iscell.npy'))
            stat = np.load(pjoin(s2ppath[0],'plane0','stat.npy'))
        else:
            print('Did not load suite2p (check: {0})'.format(s2pfolder))
    else:
        raise(OSError('No suite2p folder found [{0}]'.format(regpath)))
        
    roi_pixels = [np.vstack([x['xpix'],x['ypix']]).T for x in stat]
    neu_pixels = [x['ipix_neuropil'][0] for x in stat]

    idx = np.where((iscell[:,0]==1) & (np.abs(np.max(df_f,axis=1))<1000))[0]

    return (idx,
            plane*np.ones(idx.shape),
            np.array([roi_pixels[i] for i in idx]),
            np.array([neu_pixels[i] for i in idx]),
            df_f[idx],
            Fneu[idx],
            spks[idx])

def getROIsFromCaimanSelected(session_name,session_subname,plane,channel=0,**kwargs):
    planeFilename = 'plane{plane:03}_ch{channel:02}'
    
    regpath = pjoin(preferences['datapaths']['twophotonpaths']['reg'],
                    '{0}'.format(session_name),
                    '{0}'.format(session_subname),
                    planeFilename.format(plane=plane+1,
                                         channel=channel))
    print(regpath)
    regfiles = searchDataServers(regpath,'hdf5','results.cnmf.',verbose=True)
    if len(regfiles):
        from caiman.source_extraction.cnmf.cnmf import load_CNMF
        from caiman.utils.visualization import get_contours as get_caiman_contours
        from pandas import DataFrame 
        def get_caiman_rois(cnm):
            d1,d2 = cnm.estimates.dims
            coords = get_caiman_contours(cnm.estimates.A,[d1,d2])
            ratio = cnm.estimates.F_dff*100.
            rois = []
            idx_good = cnm.estimates.idx_components
            for i in idx_good:
                rois.append(dict(roi=coords[i]['neuron_id'],
                                 roi_pixels = coords[i]['coordinates'],
                                 neuropil_pixels = np.array([0]),
                                 neuropil = np.array([0]),
                                 df_f = ratio[i]))
            df = DataFrame.from_dict(rois)
            return df
        cnm = load_CNMF(regfiles[0])
        df = get_caiman_rois(cnm)
    else:
        regfiles = searchDataServers(regpath,'json','selected_rois.',verbose=True)
    
        #assert len(regfiles), 'No json {0} on dataservers.'.format(regpath)
        
        from pandas import read_json
        df = read_json(regfiles[0])
        print('Using the old caiman seg format.')

    roi_pixels = []
    neuropil_pixels = []
    neuropil = []
    for pix in df['roi_pixels']:
        roi_pixels.append([x for x in pix if not None in x])
    for pix in df['neuropil_pixels']:
        if None in pix:
            neuropil_pixels.append(np.array([0]))
        else:
            neuropil_pixels.append(pix)

    for pix in df['neuropil']:
        if None in pix:
            neuropil.append(np.array([0]))
        else:
            neuropil.append(pix)
    return (np.array(df['roi']),
            plane*np.ones(df['roi'].shape),
            np.array(roi_pixels),
            np.array(neuropil_pixels),
            np.stack(df['df_f']),
            neuropil)

def getROIsFromSegGUImat(session_name,session_subname,**kwargs):
    analysispath = pjoin(preferences['datapaths']['dataserverpaths'][0],
                    preferences['datapaths']['analysis'])
    data_file = pjoin(analysispath,
                     "{session_name}".format(session_name = session_name),
                     "{session_subname}".format(session_subname = session_subname),
                      'DC_segment_data.mat')

    import h5py as h5
    segdata = h5.File(data_file,'r')
    roi_nr = []
    for ref in segdata['data/ROIs/ROI_nr'][0]:
        roi_nr.append(segdata[ref][:])
    roi_nr = np.vstack(roi_nr).squeeze()
    
    plane = []
    for ref in segdata['data/ROIs/plane_nr'][0]:
        plane.append(segdata[ref][:])
    plane = np.vstack(plane).squeeze()
    roi_pixels= []
    for ref in segdata['data/ROIs/mask_neuron'][0]:
        roi_pixels.append(segdata[ref]['ir'][:])
    neuropil_pixels = []
    for ref in segdata['data/ROIs/mask_neuropil'][0]:
        neuropil_pixels.append(segdata[ref]['ir'][:])
    
    data_file = pjoin(analysispath,
                     "{session_name}".format(session_name = session_name),
                     "{session_subname}".format(session_subname = session_subname),
                      'DC_processed_traces.mat')
    tracedata = h5.File(data_file,'r')
    df_f = []
    for ref in tracedata['data/df_f']:
        df_f.append(tracedata[ref[0]][:])
    df_f = np.vstack(df_f)
    baseline = []
    for ref in tracedata['data/activity_matrix_neuropil']:
        baseline.append(tracedata[ref[0]][:])
    baseline = np.vstack(baseline)
    return roi_nr,plane,roi_pixels,neuropil_pixels,df_f,baseline

def getROIsFromMaskNeuronsMat(session_name,session_subname,**kwargs):
    
    planeFilename = 'plane{plane:03}_ch{channel:02}'
    regpath = pjoin(preferences['datapaths']['analysis'],
                    '{0}'.format(session_name),
                    '{0}'.format(session_subname))
    
    data_files = searchDataServers(regpath,'mat','timecourses.')
    if len(data_files):
        data_file = data_files[0]
    else:
        raise(OSError('No mask neurons [{0}]'.format(regpath)))

    from h5py import File as h5file
    with h5file(data_file,'r') as fd:
        df_f = fd['/ratio'].value
        baseline = fd['/baseline'].value
        cellids = fd['/cellIDs'].value.flatten()
    
    with h5file(data_file.replace('timecourses.mat',
                                  'masks_neurons.mat'),'r') as fd:
        mask = (fd['/maskNeurons'].value)
    dims = mask.shape
    roi_pixels = []
    neuropil_pixels = []
    for cid in cellids:
        roi_pixels.append(np.ravel_multi_index(np.where(mask == cid),dims=dims,order='F'))
        neuropil_pixels.append(np.array([0]))
    plane = np.array([ 0 for c in cellids])
    return cellids,plane,roi_pixels,neuropil_pixels,df_f,baseline


##################################################
############## Sparse matrices ###################
##################################################

# csc_matrix utilities
from scipy.sparse import csc_matrix
def csc_matToDict(mat,name='A'):
    name = name+'_'
    Adict = {name+'indices': mat.indices,
             name+'data':mat.data,
             name+'indptr' : mat.indptr,
             name+'dims' : np.array(mat.shape)}
    return Adict

def csc_matFromDict(di,name=None):
    # need to have at least an indices and a indptr to be a csc_matrix
    tmp0 = [x.split('_')[0] for x in filter(lambda x:('indices' in x.split('_')[-1]),di.keys())]
    tmp1= [x.split('_')[0] for x in filter(lambda x:('indptr' in x.split('_')[-1]),di.keys())]
    out = dict() 
    usedkeys = []
    for name in filter(lambda x: x in tmp1,tmp0):
        out[name] = csc_matrix((di[name + '_data'], di[name + '_indices'], di[name + '_indptr']), 
                               shape=di[name + '_dims'])
        usedkeys+= [name + '_data',name + '_dims',name + '_indices',name + '_indptr',name + '_dims']
    for name in (name for name in di.keys() if not name in usedkeys):
        out[name] = di[name]
    return out


def csc_matSavez(name,**kwargs):
    savedict = {}
    from scipy.sparse.csc import csc_matrix as csctype
    for k in kwargs.keys():
        if type(kwargs[k]) is csctype:
            savedict = dict(savedict,**csc_matrixToDict(kwargs[k]))
        else:
            savedict = dict(savedict,**{k:kwargs[k]})
    return np.savez(name,**savedict)


##################################################
############## Multiprocessing ###################
##################################################

from multiprocessing import Pool,cpu_count
from functools import partial

def runpar(f,X,nprocesses = None,**kwargs):
    ''' 
    res = runpar(function,          # function to execute
                 data,              # data to be passed to the function
                 nprocesses = None, # defaults to the number of cores on the machine
                 **kwargs)          # additional arguments passed to the function (dictionary)

    '''
    if nprocesses is None:
        nprocesses = cpu_count()
    with Pool(processes=nprocesses) as pool:
        res = pool.map(partial(f,**kwargs),X)
    pool.join()
    return res

##################################################
################ Signal ##########################
##################################################

from scipy.interpolate import interp1d
from scipy.signal import butter,filtfilt
def lowpass(x,Wfraction = 0.5,order = 5):
    ''' 
    filtered = lowpass(x,
                       Wfraction = 0.3, # filter normalized bandwith Wfraction 
                                        #  (filter freq)/(2.* sampling rate)
                        order = 5)      # order of the filter
    
    Low pass filter data or array with a butterworth zero-phase shift filter.
    '''
    B, A = butter(order, Wfraction, btype='low') # nth order Butterworth low-pass
    return filtfilt(B, A, x, axis=0)


def countEventsAboveThreshold(trace,events,time,
                              baseline = None,
                              sigma = 3,
                              lowpassW = 0.7,
                              fixedThresh = None):
    ''' 
    counts = countEventsAboveThreshold(trace,           # Trace to count events
                       events,          # Start and stop events to count between (Nx2)
                       baseline = None, # Baseline of the signal (median used if not defined)
                       sigma = 3,       # sigma stds (computed on the raw signal) above threshold
                       lowpassW = 0.7,  # Filter the signal before finding crossings
                       fixedThresh = None) # Bipass the std computation

    Count number of threshold crossings that occur between events.

    Usage with runpar:

    def countsToParFor(X,**kwargs):
        return countEventsAboveThreshold(X[0],baseline = X[1],**kwargs)
    runpar(countsToParFor,[[t,b] for t,b in zip(df_f,baseline)],events = events)

    '''
    if baseline is None: # compute the baseline if none passed
        baseline = np.nanmedian(trace)
    if not fixedThresh is None:
        thresh = fixedThresh # can use a fixed threshold
    else:
        thresh = sigma*np.std(trace) + baseline # std is computed on the raw trace; event crossing on filtered data
    if lowpassW is None:
        ftrace = trace
    else:
        ftrace = lowpass(trace,lowpassW)
    count = np.zeros(len(events))
    for i,(s,e) in enumerate(events):
        tmp = ftrace[(time>=s) & (time<e)]
        tmp = (tmp>thresh).astype(np.int8)
        tmp = np.diff(tmp)
        count[i] = np.sum(tmp > 0)
    return count

def nbTrace():
    '''This is useless but keep it'''
    from IPython.core.debugger import Tracer
    Tracer()()
