import pylab as plt
import numpy as np
# Fix figure exports
#matplotlib.rcParams['text.usetex'] = True

colors = ['#000000',
          '#d62728',
          '#1f77b4',
          '#ff7f0e',
          '#2ca02c',
          '#9467bd',
          '#8c564b',
          '#e377c2',
          '#7f7f7f',
          '#bcbd22',
          '#17becf']

mplstyledict = {'pdf.fonttype' : 42,
                'ps.fonttype' : 42,
                'svg.fonttype':'none',
                'axes.titlesize' : 12,
                'axes.labelsize' : 10,
                'axes.facecolor': 'none',
                'axes.linewidth' : 1,
                'axes.spines.right': False,
                'axes.spines.top': False,
                'axes.titleweight': 'bold',
                'axes.prop_cycle': plt.cycler('color', colors),
                'ytick.major.size': 8,
                'xtick.major.size': 8,
                'xtick.labelsize' : 8,
                'ytick.labelsize' : 8,
                'xtick.major.width': 1,
                'ytick.major.width': 1,
                'figure.edgecolor': 'none',
                'figure.facecolor': 'none',
                'figure.frameon': False,
                'font.family': ['sans-serif'],
                'font.size'  : 12,
                'font.sans-serif': ['Helvetica',
                                    'Arial',
                                    'DejaVu Sans',
                                    'Bitstream Vera Sans',
                                    'Computer Modern Sans Serif',
                                    'Lucida Grande',
                                    'Verdana',
                                    'Geneva',
                                    'Lucid',
                                    'Avant Garde',
                                    'sans-serif'],             
                'lines.linewidth' : 1.5,
                'lines.markersize' : 4,
                'image.interpolation': 'none',
                'image.resample': False,
                'savefig.dpi': 500
}
plt.matplotlib.style.use(mplstyledict)

def plotCumulative(X,bins = int(100),**kwargs):
    '''
    plotCumulative(X,bins = int(100),**kwargs)

    Plots the cumulative from a distribution of points.
    Returns : edges,cumulative, plothandle
    '''
    if type(bins) in [int,float]:
        bins = np.linspace(np.min(X),np.max(X),bins)
    cum,ed = np.histogram(np.array(X),bins = bins)
    # cumulatives go til the end of the bin
    a = plt.plot(ed[1:],np.cumsum(cum)/np.sum(cum),**kwargs)
    return ed,cum,a[0]



def plotHistogramAsLine(edges,counts,**kwargs):
    '''
    plotHistogramAsLine(edges,counts,**kwargs)

    Plots an histogram as a line. 
    Arguments are the edges and counts straight out of np.histogram.

    Returns : nx,ny, plothandle
    '''
    
    nx = np.hstack([edges[:-1],edges[1:]])
    nx = np.sort(nx)
    ny = np.zeros_like(nx)
    ny[1::2] = counts[:]
    ny[0::2] = counts[:]
    a = plt.plot(nx,ny,**kwargs)
    return nx,ny,a[0]

def plotScatterMultipleVariables(data,
                                 cliplimit = [],
                                 labels = None,
                                 color = None,
                                 spread = 0.3,
                                 markerpar = dict(clip_on = False,
                                                  markersize=2,
                                                  alpha = 0.5,
                                                  lw = 0,
                                                  markeredgecolor = 'none')):
    '''
    Plots a random scatter plot of multiple variables (randomizes x values).
        - data is a list of y values
    '''
    if not len(cliplimit):
        cliplimit = [np.min([np.nanmin(d) for d in data]),
                     np.max([np.nanmax(d) for d in data])]
    if not color is None:
        cs = color
    else:
        cs = [colors[np.mod(i,len(colors))] for i in range(len(data))]
    plts = []
    if color is None:
        color = colors
    if len(color) == 1:
        color = color*len(data)
    for i,d in enumerate(data):
        x = np.zeros(len(d)) + i + np.random.uniform(-1.*spread,spread,len(d))
        plts.append(plt.plot(x,np.clip(d,*cliplimit),'.',color=color[i],**markerpar))
    if labels is None:
        labels = [str(i) for i in np.arange(len(data))]
    plt.xticks(np.arange(len(data)),labels)
    plt.axis([-.5,len(data)-1 + .5, cliplimit[0],cliplimit[1]])
    return plts
