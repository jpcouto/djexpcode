from ipywidgets import (interact, interactive, fixed, interact_manual)
from ipywidgets import (Select, Textarea, Button,Dropdown, VBox,
                        HBox, Layout, Label, Text,Play,jslink)
from IPython.display import display
try:
    from pymysql.err import IntegrityError
except:
    pass
from .utils import readLogComments,preferences
from .pathutils import listSessions

from os.path import join as pjoin

import pylab as plt
from ipywidgets import *
from IPython.display import display
import time
import numpy as np
from ipywidgets import Play,jslink,HBox
import pandas as pd

def nbPlayStack(data,interval=30,**kwargs):
    im = plt.imshow(data[0],**kwargs)
    slider = widgets.IntSlider(0,min = 0,max = len(data)-1,step = 1,description='Frame')
    play = widgets.Play(interval=interval,
                        value=0,
                        min=0,
                        max=len(data)-1,
                        step=1,
                        description="Press play",
                        disabled=False)
    jslink((play, 'value'), (slider, 'value'))
    display(HBox([play, slider]))
    
    def updateImage(change):
        im.set_data(data[change['new']])
    slider.observe(updateImage, names='value')
    return dict(fig = plt.gcf(),ax=plt.gca(),im= im)

def nbSelectROIs(Cn,ratio,cells,selection):
    '''
Select and plot rois in the notebook.
    Inputs:
        Cn - data projection
        ratio - df-f for each ROI
        cells - list of dictionaries of the type [dict(CoM=center of mass,
                                                       coordinates= x,y roi pixels)]
        selection - dictionary of type dict(good=indexes of good rois)
    Outputs:
        The selection (input) dictionary will have a field 'selmask' that is updated by the figure.
        To get the indexes of good cells do idx_good = output(1) or idx_bad = output(0)
    Joao Couto - May 2018
    '''
    from scipy.spatial.distance import euclidean
    plt.matplotlib.style.use('ggplot')
    fig = plt.figure(figsize=[9,9])
    ax = fig.add_axes([0.0,0.3,0.9,0.7])
    ax.imshow(Cn**0.3,cmap='gray')
    ax.axis('off')
    compplot = []
    rax = fig.add_axes([0.1,0.1,0.8,0.2])
    lastidx = {'idx':0}
    dff2 = rax.plot(ratio[-1],color='gray',alpha = 0.5)[0]
    dff = rax.plot(ratio[0])[0]
    rax.set_xlabel('Frame num')
    rax.set_ylabel('df/f')
    rax.set_ylim([-10,300])

    centers = np.array([c['CoM'] for c in cells])
    indicator = ax.text(centers[0][1],centers[0][0],'0',color='orange',fontweight='bold',fontsize=8)
    indexes = selection['good']
    selection['selmask'] = np.zeros(len(cells))
    selection['selmask'][indexes] = 1

    for i,c in enumerate(cells):
        if i in indexes: 
            compplot.append(ax.plot(c['coordinates'][:,0],
                                    c['coordinates'][:,1],alpha = 1,lw = 0.5,color='y')[0])
        else:
            compplot.append(ax.plot(c['coordinates'][:,0],
                                    c['coordinates'][:,1],alpha = 1,lw = 0.5,color='r')[0])
    compplot[0].set_color('c')
    def onclick(event):
        if event.inaxes == ax:
            if selection['selmask'][lastidx['idx']]:
                compplot[lastidx['idx']].set_color('y')
            else:
                compplot[lastidx['idx']].set_color('r')
            pnt = np.array([event.ydata,event.xdata])
            idx = np.argmin([np.abs(euclidean(pnt,c)) for c in centers])
            dff.set_ydata(ratio[idx])
            dff2.set_ydata(ratio[lastidx['idx']])
            indicator.set_position([centers[idx][1],centers[idx][0]])
            indicator.set_text(str(idx))
            lastidx['idx'] = idx
            compplot[idx].set_color('c')
            if event.button == 3:
                if selection['selmask'][idx]:
                    compplot[idx].set_color('r')
                    selection['selmask'][idx] = 0
                else:
                    compplot[idx].set_color('y')
                    selection['selmask'][idx] = 1
            fig.canvas.draw()
        fig.canvas.flush_events()
    fig.canvas.mpl_connect('button_press_event', onclick)
    fig.show()
    return lambda x: np.where(selection['selmask'] == x)[0]

def nbPlotTreadmillBehavior():
    from .schemas import Behavior
    mice = np.unique(Behavior().fetch('mouse_id'))
    sessions = Behavior() & 'mouse_id = "{0}"'.format(mice[0])


    fig = plt.figure()
    ax = fig.gca()
    def sessions_select_func(change):
        session = change['new']
        dat = (Behavior() & 
           'session_name = "{0}"'.format(session)).fetch()
        ax.cla()
        for i,(n,t,v) in enumerate(dat[['session_subname','behaviortime','behaviorvelocity']]):
            ax.plot(t,v+float(i),lw=0.8)
            ax.text(0,i+0.5,n)
        plt.ylim([-0.5,len(dat)])
        plt.xlabel('time (s)')
        plt.ylabel('session * fraction belt / s')
        plt.yticks(np.arange(len(dat)))
        plt.draw()
    sessions_select_func(dict(new=sessions.fetch('session_name')[0]))
    mice_select = Dropdown(options = mice.astype(str))
    sessions_select = Select(options = 
                             np.unique(sessions.fetch('session_name')).astype(str))

    def mice_select_func(change):
        sessions = Behavior() & 'mouse_id = "{0}"'.format(change['new'])
        sessions_select.options = np.unique(sessions.fetch('session_name')).astype(str)
        sessions_select_func(dict(new=sessions.fetch('session_name')[0]))
    mice_select.observe(mice_select_func,names='value')


    sessions_select.observe(sessions_select_func,names='value')

    display(VBox([mice_select,sessions_select]))


def nbSelectTwoPhotonExperiment(afilter = '*', datapaths = None):
    ''' Select a two photon experiment from a notebook and add to datajoint database'''
    from .schemas import BrainLocation
    if datapaths is None:
        datapaths = preferences['datapaths']
    dataserverpaths = datapaths['dataserverpaths']
    twophotonpaths = datapaths['twophotonpaths']
    logpaths = datapaths['logpaths']
    
    sessiondata,sessionrundata = listSessions(afilter)

    assert len(sessiondata), 'Filter gave no result: ' + afilter
        

    
    foldersel = Select(options=sessiondata['session_name'].astype(str),
                       disabled=False)   
    folderrunsel = Select(options=sessionrundata[0]['session_subname'].astype(str),
                          disabled=False)
    comments = Textarea(value='',layout=Layout(width='500px',height='300px'),
                        disabled=False)
    def funcfoldersel(change):
        try:
            idx = change['new']['index']
            folderrunsel.options = sessionrundata[idx]['session_subname'].astype(str)
        except:
            pass
        try:
            fname = pjoin(sessiondata['session_name'][idx].astype(str),
                          sessionrundata[idx]['session_subname'].astype(str)[0])+'.log'
            fname = pjoin(dataserverpaths[0],logpaths,fname)
            try:
                comm = readLogComments(fname)
                comments.value = '\r\n'.join(comm)
            except:
                comments.value = 'Could not read file: {0}'.format(fname)
        except:
            pass

    def funcfolderrunsel(change):
        try:
            fname = pjoin(foldersel.value,change['owner'].options[change['new']['index']])+'.log'
            fname = pjoin(dataserverpaths[0],logpaths,fname)
            try:
                comm = readLogComments(fname)
                comments.value = '\r\n'.join(comm)
            except:
                comments.value = 'Could not read file: {0}'.format(fname)
        except:
            pass
    
    
    foldersel.observe(funcfoldersel)
    folderrunsel.observe(funcfolderrunsel)

    brainlocs = BrainLocation().fetch()['recording_area']
    labels = dict(mouse_id = Label(value = ''),
                  experiment_setup = Text(value = '2'),
                  recording_area = Select(options=brainlocs.astype(str)))
                  
    
    
    display(HBox([VBox([foldersel,folderrunsel]),comments]))
#    display([labels[b] for b in labels.keys()])




def nbAddMouse(name = ''):
    '''Jupyter GUI for adding a mouse.
    Example:
        nbAddMouse('JC048')
    '''
    from .schemas import Mouse,MouseStrain

    w_mouse_id = Text(description='mouse_id',
                      value=name)
    w_dob = Text(description='dob',
                 value = '2017-08-10')
    w_gender = Dropdown(description='gender',
                        options = ['M','F','U'])
    w_strain = Select(description='strain',
                      options = MouseStrain().fetch()['strain_name'])
    w_add = Button(description='Add mouse')

    def add_mouse(value):
        toadd = dict(mouse_id = w_mouse_id.value,
                     dob = w_dob.value,
                     gender = w_gender.value,
                     strain_name = w_strain.value)
        try:
            Mouse().insert1(toadd)
        except IntegrityError as err:
            print('Mouse already in database? {0}'.format(err))
        print(toadd)

    w_add.on_click(add_mouse)
    display(HBox([VBox([w_mouse_id,w_dob,w_gender,w_add]),w_strain]),title='Add mouse')



def nbPlayFacecamWithPosition(camdat,camtime,btime,bpos,bvel,laptimes = None,
                              interval=50,downsample = 3,**kwargs):
    t = btime[0]
    idx = np.where(camtime<=t)[0]
    if not len(idx):
        idx = 0
    else:
        idx = idx[-1]
    im = plt.imshow(camdat.get_frame(idx)[::downsample,::downsample],**kwargs)
    tx = plt.text(10,10,'{0:3.1f} cm \n {1:3.1f} cm/s \n lap {2}'.format(
        bpos[0],bvel[0],0),
                  fontsize = 38,color='y',va='top')
    plt.axis('off')
    slider = widgets.IntSlider(0, min = 0, max = len(btime)-1, step = 1, description='time slice',
                               continuous_update=False)
    play = widgets.Play(interval=interval,
                        value=0,
                        min=0,
                        max=len(btime)-1,
                        step=3,
                        description="Play movie",
                        disabled=False)
    jslink((play, 'value'), (slider, 'value'))
    display(HBox([play, slider]))
    
    def updateImage(change):
        idx = change['new']
        t = btime[idx]
        ii = np.where(camtime<=t)[0]
        if not len(ii):
            ii = 0
        else:
            ii = ii[-1]
        lp = 0
        if not laptimes is None:
            lp = np.where(laptimes < t)[0][-1]
        im.set_data(camdat.get_frame(ii)[::downsample,::downsample])
        tx.set_text('{0:3.1f} cm \n {1:3.1f} cm/s \n lap {2}'.format(bpos[idx],bvel[idx],lp))
        return im,tx,
    slider.observe(updateImage, names='value')
    return dict(fig = plt.gcf(),ax=plt.gca(),im= im,update = updateImage)


def nb_experiment_selection_widgets(experiments,callback = None):
    '''
    Selection widgets for experiments with an update callback to execute on selection
    
    Note: Experiments is a pandas dataframe.
    
    Usage:
    experiments = pd.DataFrame(StimulusAirPuff().proj().fetch())
    exp_selection = experiment_selection_widgets(experiments,update_plot_function);
    
    '''
    mice = experiments.mouse_id.unique()
    mice_list = widgets.Dropdown(
        options=mice,
        value=mice[0],
        description='Mouse:',
        disabled=False,
    )
    def update_sessions(mouse_id):
        return list(experiments[experiments.mouse_id == mouse_id].session_name.unique())
    sessions = update_sessions(mice[0])

    def update_runs(mouse_id,session_name):
        return list(experiments[(experiments.mouse_id == mouse_id) &
                           (experiments.session_name == session_name)].session_subname.unique())
    runs = update_runs(mice[0],sessions[0])

    sessions_list = widgets.Dropdown(
        options=sessions,
        value=sessions[0],
        description='Session:',
        disabled=False,
    )
    runs_list = widgets.Dropdown(
        options=runs,
        value=runs[0],
        description='Run:',
        disabled=False,
    )
    def exp_callback(event):
        mice_list.disabled = True
        sessions = update_sessions(mice_list.value)
        sessions_list.options = sessions
        runs = update_runs(mice_list.value,sessions_list.value)
        runs_list.options = runs
        mice_list.disabled = False
        return 

    mice_list.observe(exp_callback, names='value')
    sessions_list.observe(exp_callback, names='value')

    update_button = widgets.Button(
        description='Update',
        disabled=False,
        button_style='', # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Update',
        icon='check')
    if not callback is None:
        update_button.on_click(callback)
    display(HBox([mice_list,sessions_list,runs_list,update_button]))
    return [mice_list,sessions_list,runs_list,update_button]


def nb_interactive_airpuff_datasets():
    '''
    dict(data,plots,datapar) = nb_interactive_airpuff_datasets()
    Browse datasets for the airpuff experiments.
    Both whole TwoPhotonVolume and Traces browse.
    '''

    from .schemas import StimulusAirPuff,TwoPhotonVolume
    
    experiments = pd.DataFrame(StimulusAirPuff().proj().fetch())
    out = widgets.Output(layout={'border': '1px solid black'})
    fig = plt.figure(figsize = [5,5])
    ax = []
    ax.append(fig.add_axes([0.15,0.1,0.1,0.8]))
    for a in ax[0].get_xticklabels() + ax[0].get_yticklabels():
        a.set_fontsize(8)
    plots = []
    plots.append(plt.imshow(np.zeros([10,10]),origin = 'bottom',aspect = 'auto',cmap = 'bone_r'))
    plots.append(plt.plot(np.array([[5,5],[8,8]]).T,np.array([[0,10],[0,10]]).T,'r'))
    plots.append(plt.plot(np.nan,np.nan,'bx',clip_on=False)[0])
    dat = [[]]
    cell_slider = widgets.IntSlider(min=0,max=10,step=1,value=1)

    ax.append(fig.add_axes([0.4,0.1,0.4,0.6]))
    ax.append(fig.add_axes([0.4,0.8,0.4,0.15],sharex=ax[1]))

    datapar = dict(idx = [0])
    @out.capture()
    def plot_responses(ii):
        ii = cell_slider.value    
        idx = datapar['idx']
        i = idx[ii]
        x = np.stack(dat[0].puff_df_f.iloc[i])
        didx = dat[0].puff_index_responsive_trials.iloc[i]
        wpre = dat[0].puff_wpre.iloc[i]
        wdur = dat[0].puff_wdur.iloc[i]
        dt = dat[0].puff_dt.iloc[i]
        t = np.arange(x.shape[1])*dt
        plots[2].set_xdata(x.shape[1]*dt)
        plots[2].set_ydata(ii)
        offset = np.nanstd(x)*2
        plt.sca(ax[1])
        ax[1].cla()
        for it,X in enumerate(x):
            c = 'k'
            if it in didx:
                c = 'y'
            plt.plot(t,X-np.mean(X[:8])+offset*it,c)
    #     plt.plot(t,dat.puff_df_f.iloc[i].mean(axis = 0),'r',lw = 2)
        plt.vlines(wpre*dt + np.array([0,wdur])*dt,0,offset*len(x),color='r')
        plt.title('cell id: {0}'.format(i))
        plt.axis('off')
        plt.sca(ax[2])
        ax[2].cla()
        plt.plot(t,x.mean(axis = 0),'k',lw = 2)
        plt.vlines(wpre*dt + np.array([0,wdur])*dt,0,offset,color='r')
        plt.axis('off')
    @out.capture()
    def load_airpuff_data(event):
        print('loading: {0} {1} {2}'.format(exp_selection[0].value,
                                            exp_selection[1].value,
                                            exp_selection[2].value))
        key = dict(mouse_id = exp_selection[0].value,
                  session_name = exp_selection[1].value,
                  session_subname = exp_selection[2].value)
        for w in exp_selection:
            w.disabled = True
        dat[0] = pd.DataFrame((StimulusAirPuff()*StimulusAirPuff.Triggered()*
                               TwoPhotonVolume() & key).fetch())
        wpre = dat[0].puff_wpre.iloc[0]
        wdur = dat[0].puff_wdur.iloc[0]
        dt = dat[0].puff_dt.iloc[0]

        plots[1][0].set_xdata(wpre*dt+np.array([0,0]))
        plots[1][0].set_ydata(np.array([0,len(dat[0])]))
        plots[1][1].set_xdata((wpre+wdur)*dt+np.array([0,0]))
        plots[1][1].set_ydata(np.array([0,len(dat[0])]))

        for w in exp_selection:
            w.disabled = False
        im = np.nanmean(np.stack(dat[0].puff_df_f),axis = 1)
        im = (im.T - np.nanmean(im[:,:wpre],axis=1)).T
        idx = np.flipud(np.argsort(dat[0].puff_response_amplitude))
        datapar['idx'] = idx
        cell_slider.max = len(idx)-1
        plots[0].set_data(im[idx])
        plots[0].set_extent([0,im.shape[1]*dt,0,im.shape[0]])
        plots[0].set_clim([0,40])
        ax[0].set_title(dat[0].recording_area.iloc[0])
        ax[0].set_ylabel('{0} {1}'.format(exp_selection[1].value,exp_selection[2].value))
        return dat
    exp_selection = nb_experiment_selection_widgets(experiments,load_airpuff_data);
    load_airpuff_data(0)
    cell_slider.observe(plot_responses,'value')
    display(cell_slider,out)
    return dict(data = dat,plots=plots,datapar = datapar)

def nb_interactive_visual_datasets():
    '''
    dict(data,plots,datapar) = nb_interactive_visual_datasets()
    Browse datasets for the visual experiments.
    Both whole TwoPhotonVolume and Traces browse for each stimulus.
    '''
        
    from djexpcode.schemas import VisualStimuli,VisualStimuliStats,TwoPhotonVolume

    experiments = pd.DataFrame(VisualStimuli().proj().fetch())
    out = widgets.Output(layout={'border': '1px solid black'})
    fig = plt.figure(figsize = [8,5])
    ax = []
    ax.append(fig.add_axes([0.15,0.1,0.4,0.8]))
    for a in ax[0].get_xticklabels() + ax[0].get_yticklabels():
        a.set_fontsize(8)
    plots = []
    plots.append(plt.imshow(np.zeros([10,10]),origin = 'bottom',aspect = 'auto',cmap = 'bone_r'))
    plots.append(plt.plot(np.array([[5,5],[8,8]]).T,np.array([[0,10],[0,10]]).T,'r'))
    plots.append(plt.plot(np.nan,np.nan,'bx',clip_on=False)[0])
    dat = [[],[]]
    cell_slider = widgets.IntSlider(label='cell',min=0,max=10,step=1,value=1)
    stim_slider = widgets.IntSlider(label='stim',min=0,max=5,step=1,value=1)

    ax.append(fig.add_axes([0.7,0.1,0.2,0.6]))
    ax.append(fig.add_axes([0.7,0.8,0.2,0.15],sharex=ax[1]))

    datapar = dict(idx = [0])
    @out.capture()
    def plot_responses(ii):
        ii = cell_slider.value
        istim = stim_slider.value
        idx = datapar['idx']
        i = idx[ii]
        x = np.stack(dat[0].stim_responses.iloc[i][istim])
    #     didx = dat[0].puff_index_responsive_trials.iloc[i]
        wpre = dat[0].stim_wpre.iloc[i]
        stimes = dat[0].stim_times.iloc[0]
        sdur = np.mean(np.diff(stimes[(stimes[:,0] == 0),2:],axis = 1))
        dt = dat[0].stim_dt.iloc[i]
        wdur = sdur/dt
        t = np.arange(x.shape[1])*dt
        plots[2].set_xdata(x.shape[1]*dt)
        plots[2].set_ydata(ii)
        offset = np.nanstd(x)*2
        plt.sca(ax[1])
        ax[1].cla()
        for it,X in enumerate(x):
            c = 'k'
            plt.plot(t,X-np.mean(X[:8])+offset*it,c)
    #     plt.plot(t,dat.puff_df_f.iloc[i].mean(axis = 0),'r',lw = 2)
        plt.vlines(wpre*dt + np.array([0,wdur])*dt,0,offset*len(x),color='r')
        plt.title('cell id: {0}'.format(i))
        plt.axis('off')
        plt.sca(ax[2])
        ax[2].cla()
        plt.plot(t,x.mean(axis = 0),'k',lw = 2)
        plt.vlines(wpre*dt + np.array([0,wdur])*dt,0,offset,color='r')
        plt.axis('off')
    @out.capture()
    def load_visual_data(event):
        print('loading: {0} {1} {2}'.format(exp_selection[0].value,
                                            exp_selection[1].value,
                                            exp_selection[2].value))
        key = dict(mouse_id = exp_selection[0].value,
                  session_name = exp_selection[1].value,
                  session_subname = exp_selection[2].value)
        for w in exp_selection:
            w.disabled = True
        tmp = (VisualStimuli()*VisualStimuli.Triggered()*
                            VisualStimuliStats()*VisualStimuliStats.VisualStats()*
                            TwoPhotonVolume() & key).fetch()
        dat[0] = pd.DataFrame(tmp)
        min_datapoints = np.stack(dat[0].apply(lambda x: x.stim_df_f.shape,axis=1)).min(axis=0)[1]
        def arange_trials(stim_df_f,stimtimes,m = min_datapoints):
            rr = []
            for i,s in enumerate(np.unique(stimtimes[:,0])):
                sidx = np.where(stimtimes[:,0]==s)[0]
                rr.append(stim_df_f[sidx][:,:m])
            return rr

        dat[0]['stim_responses'] = dat[0].apply(
            lambda x: arange_trials(x.stim_df_f,x.stim_times),axis=1)

        dat[0]['stim_mean_resp'] = [np.concatenate(
            [np.nanmean(d,axis = 0) for d in dd]) for dd in dat[0].stim_responses]
        im = np.stack(dat[0]['stim_mean_resp'])
        wpre = dat[0].stim_wpre.iloc[0]
    #     wdur = dat[0].stim_wdur.iloc[0]
        dt = dat[0].stim_dt.iloc[0]


    #     plots[1][0].set_xdata(wpre*dt+np.array([0,0]))
    #     plots[1][0].set_ydata(np.array([0,len(dat[0])]))
    #     plots[1][1].set_xdata((wpre+wdur)*dt+np.array([0,0]))
    #     plots[1][1].set_ydata(np.array([0,len(dat[0])]))

        for w in exp_selection:
            w.disabled = False

        im = (im.T - np.nanmean(im[:,:wpre],axis=1)).T
        idx = np.argsort(np.argmax(np.stack(dat[0]['stim_mean_resp']),axis = 1))   
        datapar['idx'] = idx
        cell_slider.max = len(idx)-1
        stim_slider.max = len(dat[0]['stim_responses'].iloc[0])-1
        print(stim_slider.max)
        plots[0].set_data(im[idx])
        ext = [0,im.shape[1]*dt,0,im.shape[0]]
        plots[0].set_extent(ext)
        plots[0].set_clim([0,40])
        for p in plots[1]:
            p.remove()
        plots[1] = []
        startmarkers = np.cumsum(np.hstack([0,[a.shape[1] for a in dat[0].stim_responses.iloc[0]]]))
        wpre = dat[0].stim_wpre[0]
        plt.sca(ax[0])
        plots[1] = [plt.vlines(startmarkers*dt,0,len(dat[0]),color = 'y',lw = 0.3)]
        plots[1].append(plt.vlines((startmarkers+wpre)*dt,0,len(dat[0]),color = 'r',lw = 0.5))
        plt.axis(ext)
        ax[0].set_title(dat[0].recording_area.iloc[0])
        ax[0].set_ylabel('{0} {1}'.format(exp_selection[1].value,exp_selection[2].value))
        return dat
    exp_selection = nb_experiment_selection_widgets(experiments,load_visual_data);
    load_visual_data(0)
    cell_slider.observe(plot_responses,'value')
    stim_slider.observe(plot_responses,'value')

    display(cell_slider,stim_slider,out)
