from .generalschemas import *
from .twophotonschemas import *
from ..behaviorutils import *
from ..utils import removeNaN
import numpy as np

schema = dj.schema(dj.config['database.name'], locals())

@schema
class TreadmillBelt(dj.Manual):
    definition = """
    belt_id            : varchar(10)          # unique belt identifier
    ---
    belt_length        : smallint             # length of the belt
    belt_cues = NULL   : blob                 # onset and offset for each set of cues
    """

    def nbPlayFacecam(self,key,belt_len=150.):
        '''Plays the facecam movie overlaying the position and velocity to estimate cues position.'''
        import numpy as np
        facecamfiles = CuesCameraTrack().getFacecamPath(key)
        if not len(facecamfiles):
            print('Facecam data not found.')
            return
        if type(facecamfiles) in [str,np.str_]:
            # then it is a seq file
            from pims import NorpixSeq
            seq = NorpixSeq(facecamfiles)
            frametimes = (TwoPhotonPlane() & key).fetch('frametimes')[0]
            camtime = np.linspace(frametimes[0],frametimes[-1],len(seq))

        else:
            from ..imutils import TiffStack
            seq = TiffStack(facecamfiles)
            camlog = CuesCameraTrack().getCamLog(key)
            camtime = np.linspace(camlog['duinotime'].iloc[0]/1000.,camlog['duinotime'].iloc[-1]/1000.,len(seq))
        dat = (Behavior() & key).fetch()[0]
        from ..jupytertools import nbPlayFacecamWithPosition
        from ..plotutils import plt
        idx = np.where(np.diff(dat['behaviorposition'])>0)[0]
        return nbPlayFacecamWithPosition(seq,camtime,dat['behaviortime'][idx],
                                         dat['behaviorposition'][idx]*belt_len,
                                         dat['behaviorvelocity'][idx]*belt_len,
                                         laptimes = dat['laptimes'],
                                         cmap='gray',downsample = 2,aspect='equal')
        plt.axis('off')

@schema 
class TreadmillPositionExperiment(dj.Manual):
    definition = """
    -> SessionRun
    ---
    -> TreadmillBelt
    enucleated = 0     : smallint                                     # whether the mouse was enucleated  
    headpost_angle     : float                                        # deg
    shield_distance    : float                                        # cm
    led_eye            : enum("on","off","variable")                  # LED on or off
    light_conditions   : enum("light","dark","vstim)       # light conditions of the experiment
    whiskers           : enum("intact","cut","contra_cut","ipsi_cut") # whisker cutting
    whisker_block      : smallint                                     # has blocker under the whiskers
    eye_blocker        : smallint                                     # has eye blocker
    notes = ''         : varchar(4000)                                # free-text notes
    """

# Helper function
def _countsToParFor(X,**kwargs):
    from ..utils import countEventsAboveThreshold
    return countEventsAboveThreshold(X[0],fixedThresh = X[1],**kwargs)
    
@schema
class PositionMaps(dj.Computed):
    definition = """
    -> Behavior
    -> Segmentation
    -> TreadmillPositionExperiment
    ---
    velocity_map                  : longblob
    lapspace                      : longblob
    cue_times = NULL              : longblob
    cue_map_space = NULL          : longblob
    cue_velocity_map_space = NULL : longblob
    cue_velocity_map = NULL       : longblob 
    """
    velocityThreshold = 3.  # Velocity threshold for maps
    interpolationRate = 30. # Interpolation rate
    velocityFilterFreq = 8. # lowpass frequency filter for velocity
    
    active_trials_sigma_thresh = 3. # large events are above this value times the std of the trace.

    class ActivityMaps(dj.Part):
        definition = """
        -> PositionMaps
        -> Segmentation.Traces
        ---
        large_events_per_lap   : blob
        df_f_map               : longblob
        deconv_map             : longblob
        cue_maps  = NULL       : longblob
        """
    def _make_tuples(self,key):
        interrate = self.interpolationRate
        nk = (TreadmillPositionExperiment()*TreadmillBelt() & key)
        if not len(nk):
            return
        
        belt_length = nk.fetch('belt_length')[0]
        
        timebehav = (Behavior() & key).fetch('behaviortime')[0]
        posbehav =  (Behavior() & key).fetch('behaviorposition')[0]
        velbehav = (Behavior() & key).fetch('behaviorvelocity')[0]
        brate = 1./np.diff(timebehav[:2])
        laps = (Behavior() & key).fetch('laptimes')[0]
        try:
            time = np.arange(laps[0],laps[-1],1./interrate)
        except TypeError:
            return
            
        from scipy.interpolate import interp1d
        from ..utils import lowpass,runpar

        pos = interp1d(timebehav,posbehav)(time)*belt_length
        vel = interp1d(timebehav,lowpass(
            velbehav,
            Wfraction = self.velocityFilterFreq/(brate/2.)))(time)*belt_length
        lapvec = np.vstack([laps[:-1],laps[1:]]).T

        lapspace = np.arange(0,belt_length,1.)
        # Maps for the animal velocity
        X = binToLaps(lapvec,
                      time,pos,
                      np.expand_dims(vel,0),
                      velocity = vel,
                      velocityThreshold = self.velocityThreshold,
                      lapspace=lapspace)
        key['lapspace'] = lapspace
        X = removeNaN(X)
        key['velocity_map'] = X.squeeze()

        # Maps for df_f and deconv
        roinum,df_f,deconv,baseline,std = (Segmentation.Traces()
                                           & key).fetch(
                                               'roi',
                                               'df_f',
                                               'deconv',
                                               'baseline',
                                               'std')
        #print('Activity maps for {session_name} - {session_subname} - plane {plane}'.format(**key))
        # POSITION MAPS
        from scipy.interpolate import interp1d
        frametimes = (TwoPhotonPlane() & key).fetch('frametimes')[0]
        X = interp1d(frametimes,
                     np.vstack(df_f),
                     axis = -1,
                     bounds_error = False,
                     fill_value = 0)
        Xd = interp1d(frametimes,
                      np.vstack(deconv),
                      axis = -1,
                      bounds_error = False,
                      fill_value = 0)        
        X = binToLaps(lapvec,
                      time,
                      pos,X(time),
                      velocity = vel,
                      velocityThreshold = self.velocityThreshold,
                      lapspace=lapspace)
        Xd = binToLaps(lapvec,
                       time,
                       pos,
                       Xd(time),
                       velocity = vel,
                       velocityThreshold = self.velocityThreshold,
                       lapspace=lapspace)
        X = removeNaN(X)
        Xd = removeNaN(Xd)
        # EVENTS PER LAP: CAN BE MADE FASTER.
        eventsPerLap = runpar(_countsToParFor,[[t,b] for t,b in zip(df_f,
                                                                    self.active_trials_sigma_thresh*std
                                                                    +baseline)],
                              time = frametimes,
                              events = lapvec,
                              sigma = self.active_trials_sigma_thresh,
                              lowpassW = 0.7)
        # CUES IN TIME
        cues_pos = nk.fetch('belt_cues')[0]
        if not cues_pos is None:
            beltcues = np.zeros_like(pos)
            for ton,toff in lapvec:
                idx = (time>=ton) & (time<toff)
                p = pos[idx]
                b = beltcues[idx]
                for i,c in enumerate(cues_pos):
                    b[(p>=c[0]) & (p<c[1])] = i+1
                    beltcues[idx] = b

            ncues = len(np.unique(beltcues))-1
            cuetimes = []
            for c in range(ncues):
                tmp = (beltcues == c+1).astype(np.int8)
                ctime = np.where(np.diff(tmp) > 0)
                cuetimes.append(time[ctime])
                
            frate = 1./np.diff(frametimes[:2])
            wpre = int(1.*frate)
            wpost =  int(3.*frate)
            trigcue_space = np.linspace(float(wpre)*-1./frate,
                                        float(wpost)/frate,
                                        wpre+wpost)
            from ..calciumutils import getTriggeredTraces
            trigcue = [getTriggeredTraces(np.stack(df_f),
                                          c,time=frametimes,
                                          wpre = wpre,
                                          wpost = wpost)
                       for c in cuetimes]

            frate = 1./np.diff(time[:2])
            vwpre = int(1.*frate)
            vwpost =  int(3.*frate)
            trigcue_vel_space = np.linspace(float(vwpre)*-1./frate,
                                            float(vwpost)/frate,
                                            vwpre+vwpost)
            trigcue_vel = [getTriggeredTraces(np.expand_dims(vel,axis=0),c,
                                              time=time,
                                              wpre = vwpre,
                                              wpost= vwpost) for c in cuetimes]
            minreps = np.min([len(x[0]) for x in trigcue])
            for icue,x in enumerate(trigcue):
                if not len(x[0]) == minreps:
                    print('Trimming cue map [{0}] - {1}'.format(icue,key['session_name']))
                    trigcue[icue] = trigcue[icue][:,:minreps,:]
                    trigcue_vel[icue] = trigcue_vel[icue][:,:minreps,:]
                    cuetimes[icue] = cuetimes[icue][:minreps]
            trigcue = np.stack(trigcue).transpose([1,0,2,3])
            trigcue_vel = np.stack(trigcue_vel)
        else:
            trigcue = [None for i in df_f]
            trigcue_vel = None
            trigcue_vel_space = None
            trigcue_space = None
            cuetimes = None
        key['lapspace'] = lapspace
        if not cuetimes is None:
            key['cue_times'] = np.stack(cuetimes).T
        else:
            key['cue_times'] = cuetimes
        key['cue_map_space'] = trigcue_space
        key['cue_velocity_map_space'] = trigcue_vel_space
        key['cue_velocity_map'] = trigcue_vel
        #import ipdb
        #ipdb.set_trace()
        self.insert1(key)

        PositionMaps.ActivityMaps().insert([dict(key,
                                                 roi=c,
                                                 large_events_per_lap = eventsPerLap[i],
                                                 df_f_map = X[i],
                                                 deconv_map = Xd[i],
                                                 cue_maps  = trigcue[i] )
                                            for i,c in enumerate(roinum)],
                                           ignore_extra_fields=True)
        


@schema
class PositionMapsStats(dj.Computed):
    definition = """
    -> PositionMaps
    ---
    n_active_cells = 0           : smallint
    n_position_cells = 0         : smallint
    n_total_cells    = 0         : smallint
    mean_velocity_map            : longblob
    mean_cue_velocity_map = NULL : longblob
    n_trials = 0                 : smallint
    """

    # Dictates thresholds for position cells
    ev_threshold = 3
    active_trials_threshold = 0.25
    
    # Only trials with over mintrials laps are analysed.
    mintrials = 10
    
    gaussian_filter = 1.5 # This smooths the signal over +- 5 bins
    n_repeats = 100
    n_splits = 2
    
    class ActivityMapStats(dj.Part):
        definition = """
        -> PositionMapsStats
        -> PositionMaps.ActivityMaps
        ---
        position_ev = 0                           : float
        position_reliability = 0                  : float
        position_var_mean = 0                     : float
        position_fraction_responsive_trials = 0   : float
        is_position          = 0                  : smallint
        is_cue_responsive    = 0                  : smallint
        cue_amplitudes       = NULL               : blob
        cue_amplitudes_pre   = NULL               : blob
        cue_thresholds       = NULL               : blob
        cue_responsive       = NULL               : blob
        cue_pvalues          = NULL               : blob        
        mean_cue_df_f        = NULL               : longblob
        mean_position_df_f                        : longblob
        mean_position_deconv                      : longblob
        """

    def _make_tuples(self,key):
        velmap = (PositionMaps() & key).fetch('velocity_map')[0]
        ntrials = velmap.shape[0]
        if ntrials < self.mintrials:
            # Skips if not enough trials (laps in this case).
            return
        ctime = (PositionMaps() & key).fetch('cue_map_space')[0].flatten()
        cue_velocity_map = (PositionMaps() & key).fetch('cue_velocity_map')[0]

        mean_velocity_map = np.nanmean(velmap,axis=0)
        if not ctime is None:
            mean_cue_velocity_map = np.nanmean(cue_velocity_map,axis=2)

        rois,eventsPerLap,df_f_maps,deconv_maps,cue_maps = (PositionMaps().ActivityMaps()
                                                            & key).fetch('roi',
                                                                         'large_events_per_lap',
                                                                         'df_f_map',
                                                                         'deconv_map',
                                                                         'cue_maps')
        from ..calciumutils import kFoldExplainedVariance,responseReliability,trial_to_trial_correlation
        from ..calciumutils import compareToPhaseShiftThreshold
        from ..calciumutils import gaussian_filter1d, computeMeanResponse
        from ..utils import runpar

        ntotalcells = len(rois)

        # Compute and validate metrics
        with np.errstate(invalid='ignore',divide='ignore',over='ignore'):
            explained_var = np.array(runpar(kFoldExplainedVariance,deconv_maps,
                                            **dict(n_splits = self.n_splits,
                                                   n_repeats = self.n_repeats,
                                                   gaussian_filter = self.gaussian_filter)))
            reliability = np.array(runpar(trial_to_trial_correlation,deconv_maps))
            mean_df_f_map = np.nanmean(np.stack(df_f_maps),axis = 1)
            mean_deconv_map = np.nanmean(np.stack(deconv_maps),axis = 1)
            var_mean = np.var(mean_deconv_map,
                              axis=1)/np.mean(mean_deconv_map,
                                              axis=1)

        # make sure there are no impossible numbers
        explained_var[~np.isfinite(explained_var)] = 0
        explained_var[explained_var < 0] = 0 # Set negative to zero (are nonesense eitherway)
        
        reliability[~np.isfinite(reliability)] = 0
        var_mean[~np.isfinite(var_mean)] = 0
        
        fraction_responsive_trials = np.array([np.sum(p>0)/float(ntrials)
                                               for p in eventsPerLap])
        nactivecells = np.sum(fraction_responsive_trials >= self.active_trials_threshold)

        if not ctime is None:
            from scipy.stats import ks_2samp
            from scipy.stats import f_oneway
            cueresps = [computeMeanResponse(np.stack(cue_maps)[:,icue,:,:].squeeze(),
                                            ctime,
                                            stimduration = 1.,
                                            offset = 0.1, 
                                            baselinetime = -0.5,
                                            fullOutput = 1)
                        for icue in range(cue_maps[0].shape[0])]
            #cueresps = [computePeakResponse(np.stack(cue_maps)[:,icue,:,:].squeeze(),
            #                                ctime,
            #                                stimduration = 1.,
            #                                offset=1.,
            #                                baselinetime=-0.3)
            #            for icue in range(cue_maps[0].shape[0])]
            p = np.stack([p[2] for p in cueresps]).transpose(1,0,2)
            b = np.stack([b[1] for b in cueresps]).transpose(1,0,2)
            cue_amplitudes = p
            cue_amplitudes_pre = b
            mean_cue_df_f = np.nanmean(np.stack(cue_maps),axis = 2)
            
            cue_thresholds = np.zeros((p.shape[0],p.shape[1]))
            cue_responsive = np.zeros((p.shape[0],p.shape[1]))
            cue_pvalues = np.zeros_like(cue_thresholds)
            ncells = mean_cue_df_f.shape[0]
            is_cue_resp = np.zeros(ncells,dtype = int)
            ctime_freq = 1./np.diff(ctime[:2])
            is_position = np.zeros(ncells)
            for icell in np.arange(ncells):
                for icue in np.arange(mean_cue_df_f.shape[1]):
                    # NOTE: 2.5* the standard error of baseline threshold
                    cue_thresholds[icell,icue] = b[icell,icue,:].mean()+2.5*b[icell,icue,:].std()/np.sqrt(b.shape[2])
                    # NOTE: 25% samples above threshold needed for being cue responsive
                    cue_responsive[icell,icue] = int(np.sum(mean_cue_df_f[
                        icell,icue,
                        (ctime>=0.1) & (ctime<1.1)] > cue_thresholds[icell,icue])>0.25*ctime_freq[0])
                    cue_pvalues[icell,icue] = ks_2samp(b[icell,icue,:],p[icell,icue,:]).pvalue
                # NOTE: 0.05 significance threshold!
                is_cue_resp[icell] = np.sum((cue_responsive[icell]==1) & (cue_pvalues[icell]<0.05)) > 0 
                tmpamp = np.nanmean(cue_amplitudes[icell] - cue_amplitudes_pre[icell])
                # Label cells as position cells.
                # Position cells have amplitude > 2.5 and anova of cue pre and post significant at 0.05
                is_position[icell] = (tmpamp > 2.5) & (f_oneway(*[a for a in np.concatenate([cue_amplitudes[icell],
                                                                                  cue_amplitudes_pre[icell]],axis = 0)]).pvalue<0.05)

        else:
            cue_amplitudes = [None for i in range(len(rois))]
            cue_amplitudes_pre = [None for i in range(len(rois))]
            cue_thresholds  = [None for i in range(len(rois))]
            cue_pvalues  = [None for i in range(len(rois))]
            cue_responsive  = [None for i in range(len(rois))]            
            is_cue_resp  = [0 for i in range(len(rois))]
            is_position  = [0 for i in range(len(rois))]
            mean_cue_df_f = [None for i in range(len(rois))]
            mean_cue_velocity_map = None
        #is_position = ((explained_var >= self.ev_threshold) &
        #           (fraction_responsive_trials >= self.active_trials_threshold)).astype(int)
        npositioncells = np.sum(is_position)
        #print(npositioncells)
        self.insert1(dict(key,
                          n_position_cells=npositioncells,
                          n_active_cells = nactivecells,
                          n_total_cells=ntotalcells,
                          mean_velocity_map = mean_velocity_map,
                          mean_cue_velocity_map = mean_cue_velocity_map,
                          n_trials = ntrials),
                     ignore_extra_fields=True)
        keys = [dict(key,roi=roi,
                     is_position = int(is_position[i]),
                     position_ev = explained_var[i],
                     position_fraction_responsive_trials = fraction_responsive_trials[i],
                     position_var_mean = var_mean[i],
                     position_reliability = reliability[i],
                     mean_position_df_f = mean_df_f_map[i],
                     mean_position_deconv = mean_deconv_map[i],
                     is_cue_responsive = is_cue_resp[i],
                     cue_amplitudes = cue_amplitudes[i],
                     cue_amplitudes_pre = cue_amplitudes_pre[i],
                     cue_thresholds = cue_thresholds[i],
                     cue_responsive = cue_responsive[i],
                     cue_pvalues = cue_pvalues[i],
                     mean_cue_df_f  = mean_cue_df_f[i]) for i,roi in enumerate(rois)]
        PositionMapsStats().ActivityMapStats().insert(keys,ignore_extra_fields=True)

