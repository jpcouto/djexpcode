from .generalschemas import *

schema = dj.schema(dj.config['database.name'], locals())

@schema
class TwoPhotonVolume(dj.Imported):
    definition = """
    # A two-photon imaging volume
    -> SessionRun
    ---
    -> BrainLocation
    laser_wavelength   : decimal(5,1)   # (nm)
    volume_position    : blob           #
    nchannels          : smallint       # number of channels 
    nplanes            : smallint       # number of planes
    nframes            : int            # number of frames acquired
    nlines             : smallint       # number of lines
    ncols              : smallint       # number of columns
    objective          : varchar(64)    # objective name
    objective_angle    : float          # angle of the objective
    depths             : blob           # plane depths
    ptmgains           : blob           # gain of the PMTs
    magnification      : float          # magnification
    frame_rate         : float          # (Hz)
    """
    def _make_tuples(self, key):
        # use key dictionary to determine the data file path
        rawpath = pjoin(preferences['datapaths']['twophotonpaths']['raw'])
        searchpath = pjoin(rawpath,
                           "{session_name}".format(**key),
                           "{session_subname}".format(**key))
        from glob import glob
        from ..utils import getImagingFrametimes,searchDataServers
        files = searchDataServers(searchpath,'sbx')
        if len(files):
            # Scanbox data.
            sbxfile = files[0]
            from ..utils import readScanBoxInfo
            info = readScanBoxInfo(sbxfile)
            key['laser_wavelength'] = Decimal(info['laser_wavelength'])
            key['volume_position'] = np.zeros((3,))
            key['nchannels'] = info['nchannels']
            key['nplanes'] = info['nplanes']
            key['nframes'] = info['frames']
            key['nlines'] = info['nlines']
            key['ncols'] = info['ncols']
            key['objective'] = info['objective']
            key['objective_angle'] = info['objective_angle']
            key['depths'] = info['depths']
            key['ptmgains'] = info['pmtgain']
            key['magnification'] = info['magnification']
            key['frame_rate'] = info['frame_rate']
        else:
            # Data is in multiphoton format?
            files = searchDataServers(searchpath,'txt')
            if not len(files):
                raise(OSError('Volume info not found.'))
            print('Filling multiphoton values (assuming single plane).')
            with open(files[0],'r') as f:
                tmp = f.readlines()
                frate = [float(t.strip('\n').split(' ')[-1])
                         for t in tmp if 'Effective framerate' in t][0]
                magnification = np.round([1./float(t.strip('\n').split(' ')[-1])
                                          for t in tmp if 'Fast scan fraction' in t][0],1)
                nlines = [int(t.strip('\n').split(' ')[-1])
                          for t in tmp if 'Lines per frame' in t][0]
                nframes = [int(t.strip('\n').split(' ')[-1])
                           for t in tmp if 'Archived frames' in t][0]
                rotation = [float(t.strip('\n').split(' ')[-1])
                            for t in tmp if 'XZ Rotation angle' in t][0]
            key['laser_wavelength'] = Decimal(920.)
            key['volume_position'] = np.zeros((3,))
            key['nchannels'] = 1
            key['nplanes'] = 1
            key['nframes'] = nframes
            key['nlines'] = nlines
            key['ncols'] = nlines # THIS IS WRONG
            key['objective'] = 'Unknown'
            key['objective_angle'] = rotation
            key['depths'] = np.array([0])
            key['ptmgains'] = np.array([0])
            key['magnification'] = magnification
            key['frame_rate'] = frate
            
        sesnotes = (SessionRun() & key).fetch('session_subnotes')[0]
        if 'recording_area' in sesnotes:
            tmp =sesnotes.split('\n')
            for t in tmp:
                if 'recording_area' in t:
                    area = t.split( '=' )[-1].strip(' ')
        else:
            area = 'V1'        
        key['recording_area'] = area
        self.insert1(key,ignore_extra_fields=True)
        #print('Getting volume for {mouse_id} on {session_name}{session_subname}.'.format(**key))

        # Set two photon planes
        for iPlane in range(key['nplanes']):
            for iChannel in range(key['nchannels']):
                key['plane'] = iPlane
                key['channel'] = iChannel
                key['depth'] = key['depths'][iPlane]
                if iPlane == 0 and iChannel == 0:
                    #print('Getting frametimes for {mouse_id} on {session_name}{session_subname}.'.format(**key))
                    allframetimes = getImagingFrametimes(key['session_name'],
                                                         key['session_subname'],
                                                         key['nplanes'],
                                                         iPlane,iChannel)
                key['frametimes'] = allframetimes[iPlane::key['nplanes']]
                TwoPhotonPlane().insert1(key,ignore_extra_fields=True,allow_direct_insert=True)
    
@schema
class TwoPhotonPlane(dj.Imported):
    definition = """
    # Image plane within a subvolume 
    -> TwoPhotonVolume
    plane           : tinyint   # image plane within subvolume
    channel         : tinyint   # image channel
    ---
    depth           : smallint  # (um)
    frametimes      : longblob  # (s) 
    """
    planeFilename = 'plane{plane:03}_ch{channel:02}'
    
@schema 
class TwoPhotonProjection(dj.Imported):
    definition = """
    -> TwoPhotonPlane
    projtype : enum('mean','sum','std','lcorr','skewness','kurtosis')
    ---
    im       : longblob
    """
    def _make_tuples(self, key):
        # use key dictionary to determine the data file path
        nkey = key.copy()
        nkey['plane'] += 1
        datapaths = preferences['datapaths']
        rojpath = pjoin(datapaths['twophotonpaths']['regds'],
                        '{session_name}/{session_subname}'.format(**nkey))
        wildcard = 'plane{plane:03d}_ch{channel:02d}.'.format(**nkey)
        ext = 'tifproj'
        from ..utils import searchDataServers
        projpath = searchDataServers(rojpath,ext,wildcard)
        if len(projpath):
            projs = imread(projpath[0])
            keys = []
            keys.append(dict(key,projtype = 'lcorr',im=projs[0]))
            keys.append(dict(key,projtype = 'std',im=projs[1]))
            keys.append(dict(key,projtype = 'mean',im=projs[2]))
            self.insert(keys)
        else:
            print('Could not read projections for session: {session_name} - run {run_num}.'.format(**key))

@schema 
class Segmentation(dj.Imported):
    deconv_method = 'oasisAR1'
    definition = """ # Segmentation 
    -> TwoPhotonPlane
    ---
    deconvolution_method : varchar(8) 
    """
    
    class ROI(dj.Part):
        definition = """
        # Regions of interest for neurons
        -> Segmentation
        roi               :  smallint  #  region of interest within image plane
        ---
        roi_pixels        :  longblob  # pixel indices
        neuropil_pixels   :  longblob  # pixel indices
        """
    class Traces(dj.Part):
        definition = """
        # Calcium transients
        -> Segmentation.ROI
        ---
        df_f                     : longblob  # df/f
        neuropil                 : longblob  # neuropil (not the right value for now)
        denoised                 : longblob  # denoised df/f
        deconv                   : longblob  # infered rate
        baseline                 : float     # baseline
        std                      : float     # std of df/f
        deconvolution_parameters : blob      # parameters of the deconvolution
        """
        
    def _make_tuples(self, key):
        # Get the ROIS
        gotDeconv = False
        gotROIs = False
        if not key['channel'] == 0:
            print('Skipping channel {0}'.format(key['channel']))
            return
        try:
            gotROIs = True
            from ..utils import getROIsFromSegGUImat
            roi_nr,planes,roi_pixels,neuropil_pixels,df_f,baseline_ = getROIsFromSegGUImat(**key)
            iPlane = key['plane'] + 1
            indexes = np.where(planes == iPlane)[0]
            roi_nr = roi_nr[indexes]
            planes = planes[indexes]
            roi_pixels = [roi_pixels[i] for i in indexes]
            neuropil_pixels = [neuropil_pixels[i] for i in indexes]
            df_f = df_f[indexes]
            baseline_ = baseline_[indexes]
            print('Read ROIs from segGUI [{0}]'.format(key['session_name']))
        except OSError as err:
            try:
                gotROIs = True
                from ..utils import getROIsFromMaskNeuronsMat
                roi_nr,planes,roi_pixels,neuropil_pixels,df_f,baseline_ = getROIsFromMaskNeuronsMat(**key)
                print('Read ROIs from MaskNeurons')
            except OSError as err:
                try:
                    gotROIs = True
                    from ..utils import getROIsFromCaimanSelected
                    roi_nr,planes,roi_pixels,neuropil_pixels,df_f,baseline_ = getROIsFromCaimanSelected(**key)
                    print('Read ROIs from CaImAn [{0}]'.format(key['session_name']))
                except OSError as err:
                    try:
                        gotROIs = True
                        from ..utils import getROIsFromSuite2p
                        roi_nr,planes,roi_pixels,neuropil_pixels,df_f,baseline_,deconv_ = getROIsFromSuite2p(**key)
                        print('Read ROIs from suite2p [{0}]'.format(key['session_name']))
                        gotDeconv = True
                    except OSError as err:
                        print('OS error: {0} - [{1}]'.format(err,key['session_name']))
                        gotROIs = False
        if gotROIs:
            self.insert1(dict(key,deconvolution_method = self.deconv_method))
            # deconvolve
            from ..calciumutils import deconvolve
            from ..utils import runpar
            resdec = runpar(deconvolve,df_f,method = self.deconv_method)
            # add to the datajoint
            roikeys = []
            traceskeys = []
            print(len(roi_nr),len(neuropil_pixels))
            for iROI in range(len(roi_nr)):
                roikeys.append(dict(key,
                                    roi = iROI,
                                    roi_pixels = np.array(roi_pixels[iROI]).astype(int),
                                    neuropil_pixels = np.array(neuropil_pixels[iROI]).astype(int)))
                denoised, infered, baseline, param  = resdec[iROI]
                if gotDeconv:
                    infered = deconv_
                    # use the deconvolved from suite2p
                traceskeys.append(dict(key,
                                       roi = iROI,
                                       df_f = df_f[iROI],
                                       neuropil = baseline_[iROI],
                                       denoised = denoised,
                                       deconv = infered,
                                       baseline = baseline,
                                       std = np.nanstd(df_f[iROI]),
                                       deconvolution_parameters = param))

                
            Segmentation.ROI().insert(roikeys)
            Segmentation.Traces().insert(traceskeys)
            
