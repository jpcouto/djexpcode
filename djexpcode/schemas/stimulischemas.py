from .generalschemas import *
from .twophotonschemas import *

schema = dj.schema(dj.config['database.name'], locals())

@schema
class StimulusAirPuff(dj.Computed):
    """StimulusAirPuff

    Extracts traces locked to airpuff stimuli events. 

    Important: searches for 'closedLoopActuator','whiskers' or 'puff' on the 'subsession_name' to select experiments. 
               actuator onsets are from actuator0 unless actuator1 has pulses, in that case actuator1 is taken.
               extracts 1 second before and 2 seconds + the stim duration after the stimulus onset

    StimulusAirPuff contains mostly information about the number of trials, stim onsets/offsets and duration.


    StimulusAirPuff.Triggered contains triggered responses:

        * Stimuli triggered traces

            puff_deconv          : deconvolved responses [ntrials x nsamples] for each cell

            puff_df_f            : df_f responses [ntrials x nsamples] for each cell

        * Extraction parameters

            puff_wpre            : baseline duration in samples 

            puff_wpost           : post duration in samples 

            puff_wdur            : stim duration in samples 

            puff_dt              : sampling time interval

        * Statistics

            puff_avg_trial_corr
                trial-to-trial reliability
                Computed as in Rikhye, R. V, & Sur, M. (2015)
                    https://doi.org/10.1523/JNEUROSCI.1660-15.2015
                Basically the average pearson correlation of responses of trials to each other.
                This is computed only for the duration of the stimulus.

            puff_pval
                Ranksum between the samples before and those during the stimulus.

            puff_responsive_idx
                Fraction of samples during with response after the stimuli onset 

            puff_ev  
                Explained variance by the mean response.
                This is done using k-fold cross validation (2 splits and 100 repeats).

            puff_response_amplitude
                Mean response amplitude (during stim - baseline) the stim epoch considered is 0.1ms after the stimulus.
                Baseline is computed using 0.5s before the stimulus onset.

            puff_response_amplitude_shifted
                Mean response amplitude shifted by wpost.

            puff_responsive_trials
                Number of trials that has more than 2 samples above threshold during the stimulus. 
                Threshold is 3* the standard deviation of the baseline (concatenated from all trials).

            is_puff
                Whether a cell is or not responsive to the airpuff stimulus.

                Criteria are:
                    **p-value > 0.01** 
                        on a Mann-Whitney rank test between the distributions of baseline (0.5 before stim) and responses (until 0.1 after stim offset) df/f values.
                    **amplitude z-scored by baseline > 0.01**
                         df/f amplitudes on the same interval as **1** were z-scored by subtracting the mean during baseline and dividing by the standard deviation of baseline. 
"""
    t_pre = 1.5
    n_samples_threshold = 2
    b_std_thresh = 3.
    n_repeats = 100
    n_splits = 2
    default_valve = 'act0'

    definition = """
    # Air puff stimuli 
    -> TwoPhotonVolume
    ---
    valve_pulses          : longblob
    stim_onsets           : longblob       # (s)
    stim_offsets          : longblob       # (s)
    intra_pulse_frequency : longblob       # (Hz)
    puff_durations        : longblob       # (s)
    puff_duration         : float          # (s)
    n_trials              : smallint       # Number of trials
    """
    class Triggered(dj.Part):
        definition = """
        -> StimulusAirPuff
        -> Segmentation.Traces
        ---
        puff_df_f                            : longblob
        puff_deconv                          : longblob
        is_puff   = 0                        : smallint
        puff_wpre                            : smallint
        puff_wpost                           : smallint
        puff_wdur                            : smallint
        puff_dt                              : float
        puff_pvalue = 1                      : float
        puff_avg_trial_corr                  : float
        puff_ev   = 0                        : float
        puff_amplitudes = NULL               : blob
        puff_amplitudes_pre = NULL           : blob
        puff_response_amplitudes = NULL      : blob
        puff_response_amplitude = NULL       : float
        is_puff_responsive = 0               : smallint
        puff_threshold = 0                   : float
        puff_index_responsive_trials         : blob
        puff_fraction_responsive_trials      : float
        """

    def _make_tuples(self, key):
        # use key dictionary to determine the data file path
        if ('closedLoopActuator' in key['session_subname']
            or 'whiskers' in key['session_subname']
            or 'puff' in key['session_subname']):
            from ..pathutils import searchDataServers
            filename = searchDataServers(pjoin(preferences['datapaths']['logpaths'],
                                           key['session_name']),
                                     key['session_subname']+'.log','')
            if not len(filename):
                print('Logfile not found {0}'.format(key['session_name']))
            else:
                filename = filename[0]
                from pyvstim import parseVStimLog
                log,comms = parseVStimLog(filename)
                valve = 'act0'
                if 'act1' in log.keys():
                    if len(log['act1'])>10:
                        print('Using actuator 1 for {0}'.format(key['session_name']))
                        valve = 'act1'
                valve_pulses = np.array(log[valve]['duinotime'])/1000.
                frametimes = (TwoPhotonPlane() & key).fetch('frametimes')[0]
                valve_pulses = valve_pulses[(valve_pulses < frametimes[-1]-1) & (valve_pulses >= frametimes[0]+1)]
                idx_onsets = np.where(np.diff(np.hstack([-10,valve_pulses])) > 1.5)[0]
                idx_offsets = np.where(np.diff(np.hstack([valve_pulses,valve_pulses[-1]+10])) > 1.5)[0]
                stim_onsets = valve_pulses[idx_onsets]
                stim_offsets = valve_pulses[idx_offsets]
                intra_pulse_frequency = []
                stim_duration = []
                for on,off in zip(stim_onsets,stim_offsets):
                    pul = valve_pulses[(valve_pulses >= on) & (valve_pulses <= off)]
                    intra_pulse_frequency.append(1./np.nanmean(np.diff(pul)))
                    stim_duration.append(off - on)
                    
                intra_pulse_frequency = np.array(intra_pulse_frequency)
                stim_duration = np.array(stim_duration)
                if len(stim_onsets) < 5:
                    return
                self.insert([dict(key,
                                  valve_pulses = valve_pulses,
                                  stim_onsets = stim_onsets,
                                  stim_offsets = stim_offsets,
                                  puff_duration = np.max(stim_duration),
                                  puff_durations = stim_duration,
                                  intra_pulse_frequency = intra_pulse_frequency,
                                  n_trials = len(stim_onsets))],
                            ignore_extra_fields = True)
                # The planes have different frametimes so do it for each plane independently
                
                planes = (TwoPhotonPlane() & key).fetch('plane')
                for iPlane in planes:

                    frametimes = (TwoPhotonPlane() & key & 'plane = {0}'.format(iPlane)).fetch('frametimes')[0]
                    
                    roinum,df_f,deconv = (Segmentation.Traces()
                                          & key & 'plane = {0}'.format(iPlane)).fetch('roi',
                                                                                      'df_f',
                                                                                      'deconv')
                    from ..calciumutils import (getTriggeredTraces,
                                                computeMeanResponse,
                                                responsive_trials_index,
                                                kFoldExplainedVariance,
                                                trial_to_trial_correlation,
                                                responseReliability)
                    from ..utils import runpar

                    puff_dt = float(np.diff(frametimes[:2]))
                    wpre = int(np.ceil(self.t_pre/puff_dt))
                    wdur = int(np.ceil((np.round(np.max(stim_duration)*10)/10)/puff_dt))
                    wpost = int(2*wpre) + wdur
                    # Response amplitude
                    puff_df_f = getTriggeredTraces(np.stack(df_f),stim_onsets,frametimes,
                                                   wpre = wpre,
                                                   wpost = wpost)
                    puff_deconv = getTriggeredTraces(np.stack(deconv),stim_onsets,frametimes,
                                                     wpre = wpre,
                                                     wpost = wpost)
                    b_std = np.nanstd(puff_df_f[:,:,:wpre].reshape((puff_df_f.shape[0],
                                                                    puff_df_f.shape[1]*wpre)),axis = 1)
                    b_mean = np.nanmean(puff_df_f[:,:,:wpre].reshape((puff_df_f.shape[0],
                                                                      puff_df_f.shape[1]*wpre)),axis = 1)
                    responsive_trials_idx = responsive_trials_index(puff_df_f,
                                                                    wpre,
                                                                    wdur,
                                                                    b_std_thresh = self.b_std_thresh,
                                                                    n_samples_threshold = self.n_samples_threshold)
                    trialfraction = np.array([len(r) for r in responsive_trials_idx])
                    ctime = np.linspace(-1*wpre*puff_dt,
                                        wpost*puff_dt,
                                        puff_df_f.shape[2])
                    resp_amps,b,r = computeMeanResponse(puff_df_f,
                                                        ctime,
                                                        stimduration = wdur*puff_dt,
                                                        offset = 0.1, 
                                                        baselinetime = -0.5,
                                                        fullOutput = 1)
                    resp_amps = r
                    resp_amps_pre = b
                    puff_thresholds = np.zeros(r.shape[0])
                    puff_responsive = np.zeros(r.shape[0])

                    threshidx = (ctime>=0.1) & (ctime<np.max(stim_duration+0.1))
                    for icell,(rr,bb) in enumerate(zip(r,b)):
                        # NOTE: 2.5* the standard error of baseline threshold
                        puff_thresholds[icell] = np.nanmean(bb)+2.5*np.nanstd(bb)/np.sqrt(bb.shape[0])
                        # NOTE: 25% samples above threshold needed for being cue responsive
                        respduring = np.nanmean(puff_df_f[icell][:,threshidx],axis = 0)
                        puff_responsive[icell] = np.nansum( respduring > puff_thresholds[icell])>0.25*np.nanmax(
                            stim_duration)/puff_dt
                    import warnings
                    with warnings.catch_warnings():
                        warnings.filterwarnings('ignore')
                        from scipy.stats import mannwhitneyu,ks_2samp
                        pvalue = np.array([ks_2samp(rr,bb).pvalue for rr,bb in zip(r,b)])

                        ev = np.array(runpar(kFoldExplainedVariance,puff_df_f,
                                             **dict(n_splits = self.n_splits,
                                                    n_repeats = self.n_repeats)))
                        ev[~np.isfinite(ev)] = 0
                        puff_avg_trial_corr = np.array(runpar(trial_to_trial_correlation,
                                                              puff_df_f[:,:,wpre:wpre+wdur]))
                        puff_avg_trial_corr[~np.isfinite(puff_avg_trial_corr)] = 0
                    from scipy.stats import ranksums
                    
                    is_puff = (pvalue < 0.05) & (puff_responsive == 1)

                    keys = [dict((TwoPhotonPlane() & key & 'plane = {0}'.format(iPlane)).fetch(as_dict=True)[0],
                                 plane = iPlane,
                                 roi=roi,
                                 puff_wpre = wpre,
                                 puff_wpost = wpost,
                                 puff_wdur = wdur,
                                 puff_dt = puff_dt,
                                 puff_df_f = puff_df_f[i],
                                 puff_deconv = puff_deconv[i],
                                 is_puff = int(puff_responsive[i]),
                                 puff_index_responsive_trials = responsive_trials_idx[i],
                                 puff_fraction_responsive_trials = trialfraction[i]/puff_df_f.shape[1],
                                 puff_avg_trial_corr = puff_avg_trial_corr[i],
                                 puff_ev = ev[i],
                                 puff_amplitudes = resp_amps[i],
                                 puff_amplitudes_pre = resp_amps_pre[i],
                                 puff_response_amplitudes = resp_amps[i] - resp_amps_pre[i],
                                 puff_response_amplitude = np.nanmean(resp_amps[i] - resp_amps_pre[i]),
                                 puff_threshold = puff_thresholds[i],
                                 is_puff_responsive = int(is_puff[i]),
                                 puff_pvalue = pvalue[i]) for i,roi in enumerate(roinum)]
                    print('There are {0}/{1} puff responsive cells in {2}'.format(np.sum(is_puff),
                                                                                  len(is_puff),
                                                                                  key['session_name']))
                    self.Triggered().insert(keys,ignore_extra_fields=True)

        
@schema
class VisualStimuli(dj.Computed):
    wpre = 10
    wpost = 30
    definition = """
    # Air puff stimuli 
    -> TwoPhotonVolume
    ---
    stim_times            : longblob       # (code,trial,onset,offset)
    stim_duration         : longblob       # (s)
    stim_n_trials         : smallint       # Number of trials
    stim_n_stims          : smallint       # Number of stimuli
    stim_definition       : blob
    stim_options          : blob
    """
    class Triggered(dj.Part):
        definition = """
        -> VisualStimuli
        -> Segmentation.Traces
        ---
        stim_wpre           : smallint
        stim_wpost          : smallint
        stim_dt             : float
        stim_df_f           : longblob
        stim_deconv         : longblob        
        """
    def _make_tuples(self, key):
        # use key dictionary to determine the data file path
        if not ('closedLoopActuator' in key['session_subname']
                or 'light' in key['session_subname']
                or 'gray' in key['session_subname']
                or 'dark' in key['session_subname']):
            from ..pathutils import searchDataServers
            filename = searchDataServers(pjoin(preferences['datapaths']['logpaths'],
                                           key['session_name']),
                                     key['session_subname']+'.log','')
            if not len(filename):
                print('Logfile not found {0}'.format(key['session_name']))
            else:
                filename = filename[0]
                from pyvstim import getStimuliTimesFromLog
                (stimtimes,
                 stimpars,stimoptions) = getStimuliTimesFromLog(filename)
                stim_duration = np.round(np.diff(stimtimes[:,2:],axis=1))
                inskey = dict(key,
                              stim_times = stimtimes,
                              stim_duration = stim_duration,
                              stim_n_trials = stimoptions['nTrials'],
                              stim_n_stims = len(stimpars),
                              stim_definition = stimpars.to_json(),
                              stim_options = stimoptions)
                self.insert([inskey],ignore_extra_fields=True)
                #from IPython.core.debugger import set_trace
                #set_trace()
                
                # The planes have different frametimes so do it for each plane independently
                planes = (TwoPhotonPlane() & key).fetch('plane')
                for iPlane in planes:

                    frametimes = (TwoPhotonPlane() & key & 'plane = {0}'.format(iPlane)).fetch('frametimes')[0]
                    
                    roinum,df_f,deconv = (Segmentation.Traces()
                                          & key & 'plane = {0}'.format(iPlane)).fetch('roi',
                                                                                      'df_f',
                                                                                      'deconv')
                    from ..calciumutils import getTriggeredTraces
                    stim_dt = float(np.diff(frametimes[:2]))
                    wpre = int(np.ceil(1.5/stim_dt))
                    wpost = int(2.*wpre) + int(np.ceil(np.round(np.max(stim_duration))/stim_dt))
                    stim_df_f = getTriggeredTraces(np.stack(df_f),stimtimes[:,2],frametimes,
                                                   wpre = wpre,
                                                   wpost = wpost)
                    stim_deconv = getTriggeredTraces(np.stack(deconv),stimtimes[:,2],frametimes,
                                                     wpre = wpre,
                                                     wpost = wpost)
                    
                    keys = [dict((TwoPhotonPlane() & key & 'plane = {0}'.format(iPlane)).fetch(as_dict=True)[0],
                                 plane = iPlane,
                                 roi=roi,
                                 stim_wpre = wpre,
                                 stim_wpost = wpost,
                                 stim_df_f = stim_df_f[i],
                                 stim_deconv = stim_deconv[i],
                                 stim_dt = stim_dt) for i,roi in enumerate(roinum)]

                    self.Triggered().insert(keys,ignore_extra_fields=True)
                    
    def sortedStimuliResponses(self,vstim_pointer):
        if not type(vstim_pointer) is np.ndarray:
            dat = vstim_pointer.fetch()
        else:
            dat = vstim_pointer
        nstims = dat[0]['stim_n_stims'] 
        resps = [[] for i in range(nstims)]
        for icell,x in enumerate(dat):
            for i,s in enumerate(np.unique(x['stim_times'][:,0])):
                sidx = np.where(x['stim_times'][:,0]==s)[0]
                resps[i].append(x['stim_df_f'][sidx])
        resps = [np.stack(r) for r in resps]

        stimtime = []
        stimdur = []
        x = dat[0]
        for i,s in enumerate(np.unique(x['stim_times'][:,0])):
            sidx = np.where(x['stim_times'][:,0]==s)[0]
            stimdt = x['stim_dt']
            tpost = x['stim_wpost']*stimdt
            tpre = -1*x['stim_wpre']*stimdt
            sdurs = np.max(x['stim_duration'][sidx])
            stimdur.append(sdurs)
            stimtime.append(np.linspace(tpre,tpost,resps[i].shape[-1]))
        return resps,stimtime,stimdur

@schema
class VisualStimuliStats(dj.Computed):
    definition = """
    -> VisualStimuli
    ---
    n_visual_stims               : smallint       # Number of stimuli
    """
    class VisualStats(dj.Part):
        definition = """
        -> VisualStimuli.Triggered
        ---        
        stim_df_f_mean         : longblob
        stim_df_f_std          : longblob
        stim_df_f_mean_zscored : longblob
        stim_ev                : blob
        stim_amplitudes        : blob
        stim_amplitudes_pre    : blob
        is_stim_responsive     : smallint
        stim_is_responsive     : blob
        """
    def _make_tuples(self,key):
        from ..calciumutils import getTriggeredTraces,computeMeanResponse
        from ..calciumutils import kFoldExplainedVariance,trial_to_trial_correlation,responseReliability
        from ..utils import runpar
        from pandas import read_json
        visAll = (VisualStimuli()*VisualStimuli.Triggered()*TwoPhotonVolume() & key).fetch()
        uniquePlanes = np.unique(visAll['plane'])
        for iplane in uniquePlanes:
            # doing for individual planes because the rates might be rounded differently
            vis = visAll[visAll['plane'] == iplane]
            resps,stimtime,stimdur = VisualStimuli().sortedStimuliResponses(vis)

            # don't do this for puff stim
            stimdf = read_json(vis['stim_definition'][0][0])
            if 'puff' in stimdf.columns:
                vstimsidx = np.where(stimdf.puff == 0)[0]
            else:
                vstimsidx = range(len(stimdf))
            nstims = len(vstimsidx)
            # mean responses
            ptb = [computeMeanResponse(np.stack(resps[i]),stimtime[i],stimdur[i],offset = 0.1,
                                       baselinetime = -1,fullOutput = True) for i in vstimsidx]
            baselines = [p[1] for p in ptb]
            peaks = [p[2] for p in ptb]
            ptb = [p[0] for p in ptb]
            stim_df_f_mean = []
            stim_df_f_std = []
            stim_df_f_mean_zscored = []
            threshidx = (stimtime[0]>=0.1) & (stimtime[0]<np.max(vis['stim_duration'][0]+0.1))
            stim_thresholds = np.zeros([len(vis),nstims])
            stim_responsive = np.zeros([len(vis),nstims],dtype=int)
            pvalue = np.zeros([len(vis),nstims])
            is_stim_responsive = np.zeros(len(vis),dtype=int)
        
            from scipy.stats import mannwhitneyu,ks_2samp
            # explained variance (this is what takes time.)
            ev = np.zeros([len(vis),nstims])
            n_repeats = 20
            n_splits = 2
            for istim in range(nstims):
                ev[:,istim] = np.array(runpar(kFoldExplainedVariance,resps[vstimsidx[istim]],
                                              **dict(n_splits = n_splits,
                                                     n_repeats = n_repeats)))
            ev[~np.isfinite(ev)] = 0
            # compute the averages for each cell. check if responsive
            for icell,d in enumerate(vis):
                stim_df_f_mean.append(np.stack([np.nanmean(resps[r][icell],axis = 0) for r in vstimsidx]))
                stim_df_f_std.append(np.stack([np.nanstd(resps[r][icell],axis = 0) for r in vstimsidx]))
                m = np.nanmean(np.hstack([p[icell] for p in baselines]))
                s = np.nanstd(np.hstack([p[icell] for p in baselines]))
                stim_df_f_mean_zscored.append(np.stack([np.nanmean((resps[r][icell] - m)/s,axis = 0) for r in vstimsidx]))
                # NOTE: 2.5* the standard error of baseline threshold
                for istim in range(nstims):
                    b = np.hstack([b[icell] for b in baselines])
                    stim_thresholds[icell,istim] = np.nanmean(b)+2.5*np.nanstd(b)/np.sqrt(baselines[0].shape[1])
                    respduring = np.nanmean(resps[istim][icell,:,threshidx],axis = 1)
                    pvalue[icell,istim] = ks_2samp(baselines[istim][icell],peaks[istim][icell]).pvalue
                    stim_responsive[icell,istim] = np.sum( respduring > stim_thresholds[icell,istim])>0.25*np.max(vis['stim_duration'][0])/vis['stim_dt'][0]
                
                    is_stim_responsive[icell] = np.sum((stim_responsive[icell,:]>0) * (pvalue[icell,:]<0.05))>0                 
            stim_df_f_mean = np.stack(stim_df_f_mean)
            stim_df_f_std = np.stack(stim_df_f_std)
            stim_df_f_mean_zscored = np.stack(stim_df_f_mean_zscored)
            # add to schema
            toadd = []
            for icell,v in enumerate(vis):
                k = (VisualStimuli.Triggered() & v[VisualStimuli.Triggered.primary_key]).proj().fetch(as_dict = True)
                toadd.append(dict(k[0],
                                  stim_df_f_mean = stim_df_f_mean[icell],
                                  stim_df_f_std = stim_df_f_std[icell],
                                  stim_df_f_mean_zscored = stim_df_f_mean_zscored[icell],
                                  stim_ev = ev[icell],
                                  stim_amplitudes = np.stack([a[icell] for a in peaks]),
                                  stim_amplitudes_pre = np.stack([a[icell] for a in baselines]),                          
                                  is_stim_responsive = is_stim_responsive[icell],
                                  stim_is_responsive = stim_responsive[icell]))
            self.insert1(dict(key,n_visual_stims=nstims),ignore_extra_fields=True,skip_duplicates = True)
            self.VisualStats().insert(toadd,ignore_extra_fields=True)
    
    
@schema
class PuffTriggeredTwoPhotonPlane(dj.Computed):
    wpre = 10
    wpost = 30
    definition = """
    # Air puff stimuli
    -> StimulusAirPuff
    -> TwoPhotonPlane
    ---
    puff_triggered_movie : longblob
    """
