import datajoint as dj
from os.path import join as pjoin
from ..utils import *
import os
import numpy as np
from decimal import Decimal

for k in preferences['datajoint'].keys():
    dj.config[k] = preferences['datajoint'][k]

dj.conn()
schema = dj.schema(dj.config['database.name'], locals())

@schema
class MouseStrain(dj.Lookup):
    definition = """
    strain_name: varchar(20)      # unique strain name
    """
    contents = zip(['C57BL/6J',
                    'Thy1-GCaMP6s',
                    'Rorb-IRES2-Cre-D',
                    'Ai93D.R26-ZtTA',
                    'Ai93D;CaMK2a-tTA',
                    'Rorb-Ai93DCaMK2a-tTA',
                    'PVCre',
                    'Emx1Cre-Ai93D.CaMK2a-tTA',
                    'CaMKII-tTA-TRE-GCaMP6s',
                    'unknown'])
    
@schema
class Experimenter(dj.Lookup):
    definition = """
    user_id: varchar(2)
    ------
    name: varchar(20)
    mail: varchar(40)
    """
    contents = [
        ['JC','Joao Couto','joao.couto@nerf.be'],
        ['SK','Steffen Kandler','steffen.kandler@nerf.be'],
        ['EV','Elke V','joao.couto@nerf.be']]

@schema
class BrainLocation(dj.Lookup):
    definition = """
    recording_area: varchar(16)       #  
    """
    contents = zip(['V1a','Str',
                    'V1','S1','dLGN',
                    'AL','RL','AM',
                    'LI','PM','A'])

@schema
class Mouse(dj.Manual):
    ''' Experimental subject.'''
    definition = """
    mouse_id  : varchar(20)          # unique mouse id
    ---
    dob       : date                 # mouse date of birth
    gender    : enum('M', 'F', 'U')  # sex of mouse - Male, Female, or Unknown
    -> MouseStrain
    """

@schema
class ProcedureName(dj.Lookup):
    definition = """
    procedure: varchar(16)       #  
    """
    contents = zip(['headposting',
                    'injection',
                    'window replacement',
                    'handling',
                    'craniotomy'])
    
@schema
class Procedure(dj.Manual):
    ''' Surgical or behavioral manipulation. '''
    definition = """
    -> Mouse
    -> ProcedureName
    procedure_date         : date            # date
    ---
    -> Experimenter             
    metadata               : longblob        # other
    animal_weight          : decimal(2,1)   # (g)
    procedure_notes = ''   : varchar(128)    # notes
    procedure_comment = '' : varchar(4000)   # free-text notes
    """

@schema
class Session(dj.Manual):
    definition = """
    -> Mouse
    session_date       : date            # session date
    session_name       : varchar(64)     # Name of the data folder
    ---
    -> Experimenter
    experiment_type    : enum('behavior', '1P' , '2P', 'ephys','other')
    experiment_setup   : int             # experiment setup ID
    session_notes = '' : varchar(4000)   # free-text notes
    """

@schema 
class SessionRun(dj.Manual):
    definition = """
    -> Session
    run_num               : smallint      # number of the run in the session
    session_subname       : varchar(64)   # folder name for the session run
    ---
    session_subnotes = '' : varchar(4000) # free-text notes
    """

@schema
class Behavior(dj.Imported):
    definition = """
    # Behavior during the session run
    -> SessionRun 
    ---
    behaviortime = NULL     : longblob      # timebase   
    behaviorposition = NULL : longblob      # position of the animal on the track
    behaviorvelocity = NULL : longblob      # velocity of the animal
    laptimes = NULL         : longblob      # times of lap events
    licktimes = NULL        : longblob      # times of lick events
"""
    def _make_tuples(self, key):
        # use key dictionary to determine the data file path
        data_file = self.getLogPath(key)
        
        if not data_file is None:
            from ..behaviorutils import getLinearTreadmillBehaviorFromLog
            res = getLinearTreadmillBehaviorFromLog(data_file)
            if res is None:
                print('Using presentation log file parser.')
                from ..presentationlog import parsePresentationLog
                res = parsePresentationLog(data_file)
                
            if not len(res['behaviortime']):
                #print('Could not find position in log: {0}'.format(data_file))
                return
            for k in res.keys():
                import numpy as np
                if len(res[k]):
                    key[k] = res[k]
            self.insert1(key,ignore_extra_fields=True)
            print('Got behavior for {mouse_id} on {session_date}'.format(**key))
        else:
            print('File not found: {0}'.format(data_file))

    def getLogPath(self,key):
        logpath = pjoin(preferences['datapaths']['logpaths'])
        searchpath = pjoin(logpath,
                         "{session_name}".format(**key),
                         "{session_subname}.log".format(**key))
        from ..utils import getImagingFrametimes,searchDataServers
        files = searchDataServers(searchpath,'','')
        if len(files):
            return files[0]
        else:
            return None
        
    def nbPlotSessions(self):
        from ..jupytertools import nbPlotTreadmillBehavior
        import warnings
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            nbPlotTreadmillBehavior()
            

@schema
class EyeTracking(dj.Imported):
    definition = """
    # Eye tracking data
    -> SessionRun 
    ---
    eye_times            : longblob       # (s)
    eye_diameter         : longblob       # (mm)
    eye_elevation        : longblob       # (deg)
    eye_azimuth          : longblob       # (deg)
    eye_elipse           : longblob       # 
    eye_cr_pix           : longblob       # (pixels)
    eye_pupil_pix        : longblob       # (pixels)
    eye_detection_param  : blob           # (use from json)
    """
    def _make_tuples(self, key):
        # use key dictionary to determine the data file path
        data_file = self.getMptrackerAnalysedPath(key)
        log_file = Behavior().getLogPath(key)
        if not data_file is None and not log_file is None:
            from ipdb import set_trace
            set_trace()
            from pyvstim import parseVStimLog
            log,comm = parseVStimLog(log_file)
            camlog = log['cam1']
            # interpolate camtimes
            
            res = getLinearTreadmillBehaviorFromLog(data_file)
            if not len(res['behaviortime']):
                #print('Could not find position in log: {0}'.format(data_file))
                return
            for k in res.keys():
                import numpy as np
                if len(res[k]):
                    key[k] = res[k]
            self.insert1(key,ignore_extra_fields=True)



    def getMptrackerAnalysedPath(self,key,fname = 'mptrackereye.eyeh5'):
        analysispath = pjoin(preferences['datapaths']['analysis'])
        searchpath = pjoin(analysispath,
                         "{session_name}".format(**key),
                         "{session_subname}".format(**key))
        from ..utils import getImagingFrametimes,searchDataServers
        files = searchDataServers(searchpath,'',fname)
        if len(files):
            return files[0]
        else:
            return None
@schema
class CuesCameraTrack(dj.Manual):
    definition = """
    # Facecam cues data
    -> SessionRun 
    ---
    cuecam_times            : longblob       # (s)
    cuecam_pixels           : longblob       # (points)
    cuecam_intensity        : longblob       # (intensity)
    cuecam_threshold        : longblob       # (intensity)
    cuecam_binary           : longblob       # (binary)
    """


    def getCamLog(self,key,cam=2):
        log_file = Behavior().getLogPath(key)
        if not log_file is None:
            from pyvstim import parseVStimLog
            log,comm = parseVStimLog(log_file)
            camlog = log['cam{0}'.format(cam)]
        return camlog

    def getFacecamPath(self,key):
        analysispath = pjoin(preferences['datapaths']['facecampaths'])
        searchpath = pjoin(analysispath,
                         "{session_name}".format(**key),
                         "{session_subname}".format(**key))
        from ..utils import searchDataServers
        files = searchDataServers(searchpath,'tif')
        if len(files):
            return np.sort(files)
        else:
            print('Trying Norpix data.')
            searchpath = pjoin(analysispath,
                               "{session_name}".format(**key))
            files = searchDataServers(searchpath,'.seq',"{session_subname}".format(**key))
            print(files)
            if len(files):
                return files[0]
            return None
    
    def prepareCueData(self,key,cuesRegion,backgroundRegion):
        camfiles = CuesCameraTrack().getFacecamPath(key)
        from ..imutils import daskGetMeanFromTiffsRects
        res = daskGetMeanFromTiffsRects(camfiles,[cuesRegion,backgroundRegion])
        if type(res) is list:
            # normalize by background light if possible
            nx = res[0]/res[1]
        else:
            nx = res
        thresh = nx.mean() - 1.5*nx.std()
        bx = np.zeros_like(nx)
        bx[nx<thresh] = 1
        nframes = len(bx)
        camlog = self.getCamLog(key)
        camtime = np.linspace(camlog['duinotime'].iloc[0],camlog['duinotime'].iloc[-1],nframes)/1000.
        return camtime,bx,res,thresh

                    
@schema
class OnePhotonImaging(dj.Imported):

    definition = """
    # A one-photon imaging 
    -> SessionRun
    ---
    frametimes         : longblob       # (s)
    frame_rate         : float          # (Hz)
    """
    def _make_tuples(self, key):
        # use key dictionary to determine the data file path
        experiment_type = (Session() & key).fetch('experiment_type')
        if not experiment_type == '1P':
            return
        files = searchDataServers(preferences['datapaths']['onephotonpaths'])
        from ..utils import getImagingFrametimes
        files = glob(searchpath)
        if len(files):
            print('Found files')
        else:
            print('Files not found: {0}'.format(key))   
