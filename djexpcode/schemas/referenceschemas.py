from .generalschemas import *
from .twophotonschemas import TwoPhotonVolume,Segmentation
import pandas as pd

schema = dj.schema(dj.config['database.name'], locals())

@schema
class ReferenceWindow(dj.Manual):
    definition = """
    # one photon reference plane
    # Uses wfield (bitbucket)
    -> Mouse
    ref_num                : smallint
    ---
    ref_session_name       : varchar(64)   # Name of the reference experiment
    ref_session_sub_name   : varchar(64)   # Name of the reference run
    refim                  : longblob      # the reference image (uint16)
    points                 : blob          # points sampled manually around the circle
    circlepar              : blob          # circle parameters for the window
    resolution             : float         # resolution of the refim in mm/pixel
    extent                 : blob          # extent of the image to align with the allen
    """

@schema
class AllenStructures(dj.Lookup):
    definition = """
    allen_structure        : varchar(8)   # name of the structure  
    """
    contents = zip(['',
                    'RSP',
                    'SSs',
                    'SSp-bfd',
                    'SSp-tr',
                    'SSp-ll',
                    'SSp-ul',
                    'VISp',
                    'VISpm',
                    'VISl',
                    'VISal',
                    'VISrl',
                    'VISa',
                    'VISam'])

    
@schema
class AllenReferenceParameters(dj.Manual):
    definition = """
    # parameters to match to the allen
    # Uses wfield (bitbucket)
    -> ReferenceWindow
    ---
    translation         : blob
    rotation            : float
    scale               : float
    -> AllenStructures
    """

@schema
class TwoPhotonReferenceParameters(dj.Manual):
    definition = """
    # parameters to match a TwoPhotonVolume to a reference image
    # Uses wfield (bitbucket)
    -> ReferenceWindow
    -> TwoPhotonVolume
    ---
    refh                : int
    refw                : int
    rotation            : float
    scale               : float
    transpose           : bool
    ratio               : float
    origin              : blob
    dims2p              : blob # in case the FOV was cropped during registration
    """
@schema
class TwoPhotonReferenceMatch(dj.Computed):
    definition = """
    # Matched allen references to 2p planes
    -> AllenReferenceParameters
    -> TwoPhotonReferenceParameters
    -> Segmentation
    ---
    """

    class ROI(dj.Part):
        definition = '''
        # Location of an ROI in the allen reference map
        -> TwoPhotonReferenceMatch
        -> Segmentation.ROI
        ---
        roi_center          : blob
        roi_allen_center    : blob
        roi_window_center   : blob
        -> AllenStructures
        i_structure          : smallint
        distance            : float
        '''        
    def _make_tuples(self,key):
        # Goign to use a trick here are load all ROIs at once by discarding the roi number in the key
        refwin = (ReferenceWindow() & key).fetch()[0]
        refallen = (AllenReferenceParameters() & key).fetch(as_dict = True)[0]
        ref2p = (TwoPhotonReferenceParameters() & key).fetch(as_dict = True)[0]
        roisdat = (Segmentation.ROI() & key).fetch(as_dict=True)
        # get rid of nan if there were any added.
        for i,r in enumerate(roisdat):
            roisdat[i]['roi_pixels'] = r['roi_pixels'][r['roi_pixels'][:,0] > 0]
        from djexpcode.imutils import getROIcontour
        dims = ref2p['dims2p']
        rois = [getROIcontour(c['roi_pixels'],dims) for c in roisdat]
        roi_center = np.stack([c[1] for c in rois])
        from wfield import (points_to_extent,
                            adjust_points_to_reference,
                            find_point_region,
                            adjust_allen_areas,
                            points_to_allen_areas)
        from wfield.allen import load_refregions
        roi_center = adjust_points_to_reference(roi_center,dims = dims,**ref2p)
        roi_window_center = points_to_extent(roi_center,
                                             refwin['refim'].shape,
                                             refwin['extent'])
        refregions = load_refregions()
        nrefregions = adjust_allen_areas(refregions,**refallen)
        tmp = np.vstack([roi_window_center[:,1],-1*roi_window_center[:,0]]).T
        roi_allen_center = points_to_allen_areas(tmp,refregions,**refallen)
        reg = []
        for p in roi_window_center:
            reg.append(find_point_region([p[1],-1*p[0]],nrefregions))
        res = pd.DataFrame(reg,columns = ['i_structure','structure','dist']);
        keys = []
        for iROI in range(len(rois)):
            keys.append(dict(key,
                             roi_center = roi_center[iROI],
                             roi_allen_center = roi_allen_center[iROI], # FIXXXX
                             roi_window_center = roi_window_center[iROI],
                             allen_structure = res.structure.iloc[iROI],
                             i_structure = res.i_structure.iloc[iROI],
                             distance = res.dist.iloc[iROI],
                             **roisdat[iROI]))
        self.insert1(key)
        TwoPhotonReferenceMatch.ROI().insert(keys,
                          ignore_extra_fields = True)
        
        
    
    
