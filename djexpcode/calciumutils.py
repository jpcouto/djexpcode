import numpy as np
try:
    from oasis.functions import deconvolve as oasisdeconv
    from caiman.utils.visualization import get_contours as caimanGetContours
    from caiman.components_evaluation import evaluate_components_CNN as caimanEvalComponentsCNN
except:
    from skimage.measure import find_contours
    def caimanGetContours(A, dims, thr=0.9, thr_method='nrg', swap_dim=False):
        """
        THIS IS FROM CAIMAN: 
           https://github.com/flatironinstitute/CaImAn/blob/master/caiman/utils/visualization.py
        Gets contour of spatial components and returns their coordinates
        Parameters:
        -----------
        A:   np.ndarray or sparse matrix
               Matrix of Spatial components (d x K)
         dims: tuple of ints
               Spatial dimensions of movie (x, y[, z])
         thr: scalar between 0 and 1
               Energy threshold for computing contours (default 0.9)
         thr_method: [optional] string
              Method of thresholding:
                  'max' sets to zero pixels that have value less than a fraction of the max value
                  'nrg' keeps the pixels that contribute up to a specified fraction of the energy
        Returns:
        --------
        Coor: list of coordinates with center of mass and
            contour plot coordinates (per layer) for each component
        """
        if 'csc_matrix' not in str(type(A)):
            A = csc_matrix(A)
        d, nr = np.shape(A)
        # if we are on a 3D video
        if len(dims) == 3:
            d1, d2, d3 = dims
            x, y = np.mgrid[0:d2:1, 0:d3:1]
        else:
            d1, d2 = dims
            x, y = np.mgrid[0:d1:1, 0:d2:1]

        coordinates = []

        # get the center of mass of neurons( patches )
        cm = com(A, *dims)
        
        # for each patches
        for i in range(nr):
            pars = dict()
            # we compute the cumulative sum of the energy of the Ath component that has been ordered from least to highest
            patch_data = A.data[A.indptr[i]:A.indptr[i + 1]]
            indx = np.argsort(patch_data)[::-1]
            if thr_method == 'nrg':
                cumEn = np.cumsum(patch_data[indx]**2)
                # we work with normalized values
                cumEn /= cumEn[-1]
                Bvec = np.ones(d)
                # we put it in a similar matrix
                Bvec[A.indices[A.indptr[i]:A.indptr[i + 1]][indx]] = cumEn
            else:
                if thr_method != 'max':
                    warn("Unknown threshold method. Choosing max")
                Bvec = np.zeros(d)
                Bvec[A.indices[A.indptr[i]:A.indptr[i + 1]]] = patch_data / patch_data.max()

            if swap_dim:
                Bmat = np.reshape(Bvec, dims, order='C')
            else:
                Bmat = np.reshape(Bvec, dims, order='F')
            pars['coordinates'] = []
            # for each dimensions we draw the contour
            for B in (Bmat if len(dims) == 3 else [Bmat]):
                vertices = find_contours(B.T, thr)
                # this fix is necessary for having disjoint figures and borders plotted correctly
                v = np.atleast_2d([np.nan, np.nan])
                for _, vtx in enumerate(vertices):
                    num_close_coords = np.sum(np.isclose(vtx[0, :], vtx[-1, :]))
                    if num_close_coords < 2:
                        if num_close_coords == 0:
                            # case angle
                            newpt = np.round(old_div(vtx[-1, :], [d2, d1])) * [d2, d1]
                            vtx = np.concatenate((vtx, newpt[np.newaxis, :]), axis=0)
                        else:
                            # case one is border
                            vtx = np.concatenate((vtx, vtx[0, np.newaxis]), axis=0)
                    v = np.concatenate(
                        (v, vtx, np.atleast_2d([np.nan, np.nan])), axis=0)

                pars['coordinates'] = v if len(
                    dims) == 2 else (pars['coordinates'] + [v])
            pars['CoM'] = np.squeeze(cm[i, :])
            pars['neuron_id'] = i + 1
            coordinates.append(pars)
        return coordinates

    def com(A, d1, d2, d3=None):
        """Calculation of the center of mass for spatial components
        Inputs:
        ------
        A:   np.ndarray
          matrix of spatial components (d x K)
        d1:  int
          number of pixels in x-direction
        d2:  int
          number of pixels in y-direction
        d3:  int
          number of pixels in z-direction
        Output:
        -------
        cm:  np.ndarray
          center of mass for spatial components (K x 2 or 3)
        """

        if 'csc_matrix' not in str(type(A)):
            A = scipy.sparse.csc_matrix(A)
    
        if d3 is None:
            Coor = np.matrix([np.outer(np.ones(d2), np.arange(d1)).ravel(),
                              np.outer(np.arange(d2), np.ones(d1)).ravel()], dtype=A.dtype)
        else:
            Coor = np.matrix([
                np.outer(np.ones(d3), np.outer(np.ones(d2), np.arange(d1)).ravel()).ravel(),
                np.outer(np.ones(d3), np.outer(np.arange(d2), np.ones(d1)).ravel()).ravel(),
                np.outer(np.arange(d3), np.outer(np.ones(d2), np.ones(d1)).ravel()).ravel()],
                             dtype=A.dtype)
        cm = (Coor * A / A.sum(axis=0)).T
        return np.array(cm)

from scipy.sparse import csc_matrix
from scipy.stats import zscore
from scipy.ndimage.filters import gaussian_filter1d
import warnings

def deconvolve(traces,method = "oasisAR1"):
    '''
    denoised, infered, baseline, param  = deconvolve(traces)
    '''
    if method == "oasisAR2":
        denoised = []
        infered = []
        baseline = []
        param = []

        if len(traces.shape) > 1:
            for trace in traces:
                with warnings.catch_warnings():
                    warnings.filterwarnings("ignore",category=FutureWarning)
                    d, i, b, p, lam = oasisdeconv(trace.astype(np.float64),
                                                  g=(None,None),
                                                  penalty=1,
                                                  optimize_g=5)
                    denoised.append(d)
                    infered.append(i)
                    baseline.append(b)
                    param.append(p)
        else:
            with warnings.catch_warnings():
                warnings.filterwarnings("ignore",category=FutureWarning)

                denoised, infered, baseline, param, lam = oasisdeconv(traces.astype(np.float64),
                                                                      g=(None,None),
                                                                      penalty=1,
                                                                      optimize_g=5)
    elif method == "oasisAR1":
        denoised = []
        infered = []
        baseline = []
        param = []
        if len(traces.shape) > 1:
            for trace in traces:
                with warnings.catch_warnings():
                    warnings.filterwarnings("ignore",category=FutureWarning)

                    d, i, b, p, lam = oasisdeconv(trace.astype(np.float64),
                                                  g=(None,),
                                                  penalty=1,
                                                  optimize_g=5)
                    denoised.append(d)
                    infered.append(i)
                    baseline.append(b)
                    param.append(p)
        else:
            with warnings.catch_warnings():
                warnings.filterwarnings("ignore",category=FutureWarning)

                denoised, infered, baseline, param, lam = oasisdeconv(traces.astype(np.float64),
                                                                      g=(None,),
                                                                      penalty=1,
                                                                      optimize_g=5)
    else:
        raise "Unknown deconvolution method."
    return denoised, infered, baseline, param

from scipy.cluster.hierarchy import linkage,dendrogram,fcluster
def sortByHierarchy(X,method = 'ward',max_d = None):
    if max_d is None:
        max_d = len(X)/3
    Z = linkage(X,method=method,optimal_ordering=True)
    clusters = fcluster(Z, max_d, criterion='maxclust')
    return np.argsort(np.abs(clusters)),clusters


def saveROISelection(fname,idx_good,coords,ratio):
    rois = []
    coords[0]
    for i in idx_good:
        rois.append(dict(roi=coords[i]['neuron_id'],
                         roi_pixels = coords[i]['coordinates'],
                         neuropil_pixels = np.array([0]),
                         neuropil = np.array([0]),
                         df_f = ratio[i]))
    from pandas import DataFrame,read_json
    df = DataFrame.from_dict(rois)
    df.to_json(fname)

def responsive_trials_index(X,wpre,wdur,thresh = None,b_std_thresh = 3., n_samples_threshold = 2):
    '''
    Computes the index of the trials aboev b_std_thresh above baseline.
    returns a list of indexes.
    Usage: 
    resp_trials = responsive_trials_index(nx,dat.puff_wpre.iloc[0],dat.puff_wdur.iloc[0])
    trialfraction = np.array([len(r) for r in resp_trials])/nx.shape[1]

    '''
    if len(X.shape) == 2:
        X = [X]
    res = []
    if thresh is None:
        recompute_thresh = True
    for x in X:
        b_std = np.nanstd(x[:,:wpre])
        b_mean = np.nanmean(x[:,:wpre])
        if recompute_thresh:
            thresh = b_std_thresh*b_std + b_mean
        res.append(np.where(np.sum(x[:,wpre:wpre+wdur] > thresh,axis = 1) > n_samples_threshold)[0])
    if len(res)>1:
        return res
    else:
        return res[0]
    
def responseReliability(dd,axis=0):
    ''' Response reliability computed as in:
        
            Rikhye, R. V, & Sur, M. (2015). 
        Spatial Correlations in Natural Scenes Modulate Response Reliability in Mouse Visual Cortex. 
            Journal of Neuroscience
        The response reliability "is the average correlation of all pairwise combinations of trials for a single" stimulus.
    
    Takes dd as input: [nTrials x Mpoints]
    Example:
        reliability = np.array([responseReliability(dd) for dd in data['deconv_map']])

        
    '''
    assert len(dd.shape) == 2, "This only works for 2d matrices."
    T = float(dd.shape[axis])
    rowvar = True
    if axis:
        rowvar = False
    
    tmp = np.corrcoef(dd,rowvar=rowvar)
    idx = np.triu_indices(len(tmp),1)
    
    return 2.*np.nansum(np.abs(tmp[idx]))/(T**2 - T)

def discardNanTrials(X):
    if len(X.shape) == 3:
        x = X[0]
        return X[:,~np.isnan(np.sum(x,axis=1)),:]
    else:
        return X[~np.isnan(np.sum(X,axis=1)),:]
    
def trial_to_trial_correlation(X,discardNan = True):
    if discardNan:
        tmp = discardNanTrials(X)
    else:
        tmp = X
    idx = np.triu_indices(tmp.shape[1], k=1)
    tmp = np.corrcoef(tmp,rowvar=0)
    return np.nanmean(tmp[idx])

def getTriggeredTraces(traces,events,time = None, wpre = 10,wpost=30):
    if time is None:
        time = np.linspace(traces.shape[1])
    trigTraces = np.zeros([traces.shape[0],len(events),wpre+wpost])
    trigTraces[:] = np.nan
    index = np.arange(-1*wpre,wpost)
    for i,ev in enumerate(events):
        idx = np.where(time < ev)[0]
        if not len(idx) or traces.shape[1] < idx[-1] + wpost:
            print('Skipped an event at {0}'.format(ev))
            continue
        idx = idx[-1]
        trigTraces[:,i,:] = traces[:,idx+index]
    return trigTraces

def computePeakResponse(responses,rtime,stimduration,offset = None,baselinetime = None,fullOutput = False):
    ''' 
    responses is a [cells x trials x samples] array
    Assumes stim time is at rtime == 0
    '''
    if offset is None:
        offset = 0
    if baselinetime is None:
        baselinetime = rtime[0]
    baselines = np.nanmean(responses[:,:,(rtime>baselinetime) & (rtime<=0)],axis = 2)
    peaks = np.nanmax(responses[:,:,(rtime<=stimduration + offset) & (rtime>0)],axis = 2)
    if not fullOutput:
        return np.nanmean(peaks - baselines,axis = 1)
    else:
        return np.nanmean(peaks - baselines,axis = 1),baselines,peaks

def computeMeanResponse(responses,rtime,stimduration,
                        offset = None,
                        baselinetime = None,
                        fullOutput = False):
    ''' 
    responses is a [cells x trials x samples] array
    Assumes stim time is at rtime == 0
    '''
    if offset is None:
        offset = 0
    rtime = rtime.flatten()
    if baselinetime is None:
        baselinetime = rtime[0]
    print(rtime.shape,responses.shape)
    baselines = np.nanmean(responses[:,:,(rtime>baselinetime) & (rtime<=0)],axis = 2)
    peaks = np.nanmean(responses[:,:,(rtime<=stimduration + offset) & (rtime>0)],axis = 2)
    import warnings
    with warnings.catch_warnings() as warn:
        warnings.simplefilter('ignore')
        ptb = np.nanmean(peaks - baselines,axis = 1)
    if not fullOutput:
        return ptb
    else:
        return ptb,baselines,peaks

    
def explainedVariance(r,m,method = 'ev'):
    '''
     expvar = explainedVariance(response,model,method = 'ev'|'pv|)
   r is response matrix (nTrials x nResponse)
   m is model response (1 x nResponse) or (nTrials x nResponse)
       ev is fraction of variance in responses explained by the model
       pv is fraction of explainable variance in responses explained by model
   see Linden and Sahani NIPS paper
    
    Translated from the Bonin lab core tools.
    
    '''
    Pr = np.nanmean(np.nanvar(r,axis=1))   # response power
    Pe = np.nanmean(np.nanvar(r-m,axis=1)) # error power
    if method == 'pv':
        nTrials = float(r.shape[0])
        Pa = np.nanvar(np.nanmean(r,axis = 0)) 
        Pn = (Pr-Pa)*nTrials/(nTrials-1);
        return (Pr-Pe)/(Pr-Pn)*100.0
    elif method == 'ev':
        return (Pr - Pe)/Pr*100.

def kFoldExplainedVariance(data,model = lambda x:np.nanmean(x,axis = 0),
                           n_splits = 5, n_repeats = 20, gaussian_filter=None,
                           method = 'ev'):
    '''
    ev_cross_validated = kFoldExplainedVariance(data,
                                                model= lambda x:np.nanmean(x,axis = 0) # model function
                                                n_splits = 4,         # number of folds
                                                n_repeats = 10,       # number of repetitions
                                                gaussian_filter=None, # same as no filter
                                                method = 'ev')
    Performs cross-validation by splitting the dataset randomly "n_repeats" times in "n_splits" blocks.
    If "gaussian_filter" is used the data are filtered with a gaussian kernel of size "gaussian_filter".
    Returns the average explained variance accross splits where the explained variance was numeric.
    If no value was numeric, returns zero. 
    Negative returns are clipped to zero.
    '''
    from sklearn.model_selection import RepeatedKFold as KFold
    kf = KFold(n_splits=n_splits,n_repeats=n_repeats)
    if not gaussian_filter is None:
        X = gaussian_filter1d(data,gaussian_filter)
    else:
        X = data
    ev = []
    for train, test in kf.split(X):
        model_data = model(X[train])
        test_data = X[test]
        ev.append(explainedVariance(test_data,model_data,method = method))
    ev = np.array(ev)
    idx = np.isfinite(ev)
    if not np.sum(idx):
        return 0
    ev = np.nanmean(ev[np.isfinite(ev)])
    return  ev if ev>0 else 0. 
    
def shuffle_mean_rmse(resp,maxRespFractionOfTrials = 4.):
    dresp = resp.copy()
    dresp[dresp > dresp.shape[0]/maxRespFractionOfTrials] = dresp.shape[0]/maxRespFractionOfTrials
    ndresp = dresp.copy()
    for d in ndresp:
        np.random.shuffle(d)
    return np.sqrt(np.sum((np.nanmean(ndresp,axis = 0)-np.nanmean(dresp,axis = 0))**2))/dresp.shape[0]

def compareToPhaseShiftThreshold(resp,nsigmas = 2, samplesize = 1000):
    resp = resp.copy()
    resp[resp>resp.shape[0]/10] = resp.shape[0]/10
    m = np.nanmean(resp,axis = 0)
    s = shift_rows(resp[np.random.choice(resp.shape[0],size = samplesize,replace = True)])
    s = np.nanmean(s,axis = 0) + np.nanstd(s,axis = 1).mean()
    m[m<s] = np.nan
    return np.sqrt(np.nansum((s-m)**2))

def compareToPhaseShiftThreshold(resp,nsigmas = 0.9, samplesize = 1000):
    m = np.nanmean(resp,axis = 0)
    s = shift_rows(resp[np.random.choice(resp.shape[0],size = 1000,replace = True)])
    sigma = np.nanstd(s,axis = 1).mean()
    return np.sum(m>(m.mean()+sigma*nsigmas))

def shift_rows(data, max_shift = None):
    """Left-shifts each row in `data` by a random amount up to `max_shift`.
    Adapted from StackOverFlow.
    """
    if max_shift is None:
        max_shift = data.shape[1]
    return np.array([np.roll(row, -np.random.randint(0, max_shift)) for row in data])


def sort_rastermap(raster,smooth_space = False,smooth_time = False,zscore_norm = True):
    ''' Sort responses based on similarity using Stringer's toolbox.'''
    from rastermap import mapping
    # user options
    S = np.stack(raster,axis=0)
    isort1,isort2 = mapping.main(S,{'nclust': 50, # number of clusters
                                    'iPC': np.arange(0,200).astype(np.int32), # number of PCs to use for mapping algorithm
                                    'upsamp': 100, # upsampling factor for embedding position
                                    'sigUp': 1, # standard deviation for kriging upsampling
                                    'equal': False # whether or not clusters should be of equal size (recommended False!)
                                   })
    Sm = S[isort1,:]
    if smooth_space:
        Sm = gaussian_filter1d(S[isort1,:].T, np.minimum(10,int(S.shape[0]*0.005)), axis=1).T
    if zscore_norm:
        Sm = zscore(Sm, axis=1)
    if smooth_time:
        Sm = gaussian_filter1d(Sm, 1, axis=1)
    
    return Sm,isort1
