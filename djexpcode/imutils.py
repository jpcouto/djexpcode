import numpy as np
import cv2
from tifffile import TiffFile,imread
import os
from glob import glob

def makeBinaryImageMask(idx,dims):
    '''
    dims = (key['ncols'],key['nlines'])
    '''
    mask = np.zeros(dims,dtype=bool)
    if len(idx.shape)<2:
        xc,yc = np.unravel_index(idx,[dims[1],dims[0]])
    else:
        xc,yc = idx.T
    mask[yc,xc] = 1
    return mask
#def makeBinaryImageMask(idx,dims):
#    '''
#    dims = (key['ncols'],key['nlines'])
#    '''
#    mask = np.zeros(dims,dtype=bool)
#    xc,yc = np.unravel_index(idx,dims)
#    mask[xc,yc] = 1
#    return mask

def resize_rescale_rotate(im,xy,resizefactor,ratio,angle,resdims):
    res_h,res_w = resdims
 

    # Scaling parameter
    s = resizefactor*2
    oy,ox = xy
    (h, w) = im.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    Mrot = cv2.getRotationMatrix2D((cX, cY), angle, 0.5)
    
    result = cv2.warpAffine(im, Mrot, (w, h))
    
    M = np.c_[[s, 0, ox],
              [0, s/ratio, oy]].T

    return cv2.warpAffine(result, M, (res_w, res_h))

def getROIcontour(roipixels,dims,transforms = None):
    if len(roipixels.shape) > 1:
        roipixels = roipixels[roipixels[:,0] > 0] # indices are positive
    mask = makeBinaryImageMask(roipixels,dims).astype(np.uint8)
    if not transforms is None:
        p,mag,rsize = transforms
        mask = cv2.dilate(mask,np.ones([10,10]).astype(np.uint8)) # Dilate because otherwhise these become just points
        mask = resize_rescale_rotate(mask,p,mag[0],mag[1],mag[2],rsize)
        mask[mask > 0] = 128
    _, contours, _ = cv2.findContours(mask.astype(np.uint8).copy(),
                                      cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cont = np.squeeze(contours[0])
    c = np.vstack([cont,cont[0,:]])
    
    M = cv2.moments(contours[0])
    if not M['m00'] == 0:
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        center = np.array([cX,cY])
    else:
        center = [contours[0][0,0,0],contours[0][0,0,1]]
    return c,center


def saveAVI(fname,data,frame_rate = 30.,codec='MJPG',norm = None):
    fourcc = cv2.VideoWriter_fourcc(*codec)
    out = cv2.VideoWriter(fname,fourcc, frame_rate, (data.shape[2],data.shape[1]))
    for frame in tqdm(data,desc='Writing avi'):
        nframe = frame.copy()
        if not norm is None:
            nframe = cv2.convertScaleAbs(nframe,
                                         alpha = 255./np.abs(norm).sum(),
                                         beta = np.abs(norm[0]))
        if len(nframe.shape) == 2:
            frame = cv2.cvtColor(nframe,cv2.COLOR_GRAY2RGB)
        out.write(frame)
    out.release()


def selectRectangle(ax):
    from matplotlib.widgets import RectangleSelector
    return RectangleSelector(ax,lambda x:x , interactive=True)

def local_corr(block):
    """Computes the correlation image for the input dataset Y  using a faster FFT based method
    Parameters:
    -----------
    Y:  np.ndarray (3D or 4D)
        Input movie data in 3D or 4D format
    eight_neighbours: Boolean
        Use 8 neighbors if true, and 4 if false for 3D data (default = True)
        Use 6 neighbors for 4D data, irrespectively
    swap_dim: Boolean
        True indicates that time is listed in the last axis of Y (matlab format)
        and moves it in the front
    Returns:
    --------
    Cn: d1 x d2 [x d3] matrix, cross-correlation with adjacent pixels
    
    
    This is from CaIman: https://github.com/simonsfoundation/CaImAn/blob/master/caiman/summary_images.py
    """

    Y = np.array(block).astype('float32')
    Y -= np.nanmean(Y,axis = 0)
    Y /= np.nanstd(Y,axis = 0)

    sz = np.ones((3,3),dtype='float32')
    sz[1,1] = 0
    from scipy.ndimage.filters import convolve
    Yconv = convolve(Y,sz[np.newaxis,:],mode='constant')
    MASK = convolve(np.ones(Y.shape[1:],dtype='float32'),sz,mode='constant')
    Cn =  (np.nanmean(Yconv*Y,axis=0)/MASK).reshape([1]+[a for a in Y.shape[1:]]).astype(np.float32)
    return Cn

class TiffStack(object):
    def __init__(self,filenames):
        assert type(filenames) in [list,np.ndarray], 'Pass a list of filenames.'
        self.filenames = filenames
        for f in filenames:
            assert os.path.exists(f), f + ' not found.'
        # Get an estimate by opening only the first and last files
        framesPerFile = []
        self.files = []
        for i,fn in enumerate(self.filenames):
            if i == 0 or i == len(self.filenames)-1:
                self.files.append(TiffFile(fn))
            else:
                self.files.append(None)                
            f = self.files[-1]
            if i == 0:
                dims = f.series[0].shape
                self.shape = dims
            elif i == len(self.filenames)-1:
                dims = f.series[0].shape
            framesPerFile.append(np.int64(dims[0]))
        self.framesPerFile = np.array(framesPerFile, dtype=np.int64)
        self.framesOffset = np.hstack([0,np.cumsum(self.framesPerFile[:-1])])
        self.nFrames = np.sum(framesPerFile)
        self.curfile = 0
        self.curstack = self.files[self.curfile].asarray()
        N,self.h,self.w = self.curstack.shape[:3]
        self.dtype = self.curstack.dtype
        self.shape = (self.nFrames,self.shape[1],self.shape[2])
    def getFrameIndex(self,frame):
        '''Computes the frame index from multipage tiff files.'''
        fileidx = np.where(self.framesOffset <= frame)[0][-1]
        return fileidx,frame - self.framesOffset[fileidx]
    def __getitem__(self,*args):
        index  = args[0]
        if not type(index) is int:
            Z, X, Y = index
            if type(Z) is slice:
                index = range(Z.start, Z.stop, Z.step)
            else:
                index = Z
        else:
            index = [index]
        img = np.empty((len(index),self.h,self.w),dtype = self.dtype)
        for i,ind in enumerate(index):
            img[i,:,:] = self.getFrame(ind)
        return np.squeeze(img)

    def get_frame(self,frame):
        return self.getFrame(frame)
    
    def getFrame(self,frame):
        ''' Returns a single frame from the stack '''
        fileidx,frameidx = self.getFrameIndex(frame)
        if not fileidx == self.curfile:
            if self.files[fileidx] is None:
                self.files[fileidx] = TiffFile(self.filenames[fileidx])
            self.curstack = self.files[fileidx].asarray()
            self.curfile = fileidx
        return self.curstack[frameidx,:,:]
    def __len__(self):
        return self.nFrames


def daskLoadTiffs(files):
    from dask import array as da
    from dask import delayed
    
    delayed_imread = delayed(imread, pure=True)  # Lazy version of imread
    # To load when the last file has a different dimension
    sample0 = imread(files[0])
    sample1 = imread(files[-1])

    lazy_values = []
    for f in files[:-1]:
        lazy_values.append([delayed_imread(f),sample0.shape])
    lazy_values.append([delayed_imread(files[-1]),sample1.shape])
    
    arrays = [da.from_delayed(v0,        
                              dtype=sample0.dtype, 
                              shape=v1)
              for v0,v1 in lazy_values]
    return da.concatenate(arrays, axis=0)

def daskGetMeanFromTiffsRects(files, rects):
    datiff = daskLoadTiffs(files)
    from dask.diagnostics import ProgressBar
    import dask.multiprocessing
    from dask import compute
    comp = []
    for r in rects:
        xi = (int(np.min(r.corners[1])),int(np.max(r.corners[1])))
        yi = (int(np.min(r.corners[0])),int(np.max(r.corners[0])))
        comp.append(datiff[:,xi[0]:xi[1],yi[0]:yi[1]].mean(axis = [1,2]))
    with ProgressBar():
        results = compute(comp,get=dask.multiprocessing.get)
    return results[0]

def rotate_along_axis(img,nh = None,nw = None, theta=0, phi=0, gamma=0, dx=0, dy=0, dz=0):
    """ Wrapper of Rotating a Image """
    from numpy import pi
    # Get radius of rotation along 3 axes
    def get_rad(theta, phi, gamma):
        return (deg_to_rad(theta),
                deg_to_rad(phi),
                deg_to_rad(gamma))

    def get_deg(rtheta, rphi, rgamma):
        return (rad_to_deg(rtheta),
                rad_to_deg(rphi),
                rad_to_deg(rgamma))

    def deg_to_rad(deg):
        return deg * pi / 180.0

    def rad_to_deg(rad):
        return deg * 180.0 / pi
        """ Get Perspective Projection Matrix """
    def get_M(w,h,f, theta, phi, gamma, dx, dy, dz):

        # Projection 2D -> 3D matrix
        A1 = np.array([ [1, 0, -w/2],
                        [0, 1, -h/2],
                        [0, 0, 1],
                        [0, 0, 1]])

        # Rotation matrices around the X, Y, and Z axis
        RX = np.array([ [1, 0, 0, 0],
                        [0, np.cos(theta), -np.sin(theta), 0],
                        [0, np.sin(theta), np.cos(theta), 0],
                        [0, 0, 0, 1]])

        RY = np.array([ [np.cos(phi), 0, -np.sin(phi), 0],
                        [0, 1, 0, 0],
                        [np.sin(phi), 0, np.cos(phi), 0],
                        [0, 0, 0, 1]])

        RZ = np.array([ [np.cos(gamma), -np.sin(gamma), 0, 0],
                        [np.sin(gamma), np.cos(gamma), 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1]])

        # Composed rotation matrix with (RX, RY, RZ)
        R = np.dot(np.dot(RX, RY), RZ)

        # Translation matrix
        T = np.array([  [1, 0, 0, dx],
                        [0, 1, 0, dy],
                        [0, 0, 1, dz],
                        [0, 0, 0, 1]])

        # Projection 3D -> 2D matrix
        A2 = np.array([ [f, 0, w/2, 0],
                        [0, f, h/2, 0],
                        [0, 0, 1, 0]])

        # Final transformation matrix
        return np.dot(A2, np.dot(T, np.dot(R, A1)))
    rtheta, rphi, rgamma = get_rad(theta, phi, gamma)

    # Get ideal focal length on z axis
    # NOTE: Change this section to other axis if needed
    h = img.shape[0]
    w = img.shape[1]
    d = np.sqrt(h**2 + w**2)
    dz  = d / (2 * np.sin(rgamma) if np.sin(rgamma) != 0 else 1)
    
    
    # Get projection matrix
    mat = get_M(w,h,dz,rtheta, rphi, rgamma, dx, dy, dz)

    return cv2.warpPerspective(img.copy(), mat, (nh, nw))

