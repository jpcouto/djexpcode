# Tools to access and process presentation log files

import pandas as pd
try:
    from cStringIO import StringIO
except:
    try:
        from StringIO import StringIO
    except ImportError:
        from io import StringIO

import sys
import numpy as np
import os
from glob import glob

def parsePresentationLog(logfile):
    '''
    USAGE:
    log = parsePresentationLog(logfile)
    simplified version to parse logs from presentation (Bonin lab -  Behavioral Systems)

    Joao Couto - May 2018
    '''
    if not os.path.exists(logfile):
        raise(ValueError('File '+ logfile +' does not exist.'))

    generallog = ''
    picturelog = ''
    manuallog = []
    # Parse logfile
    with open(logfile,'r') as f:
        text = f.readline()
        scenario = text.split('-')[1].strip()
        text = f.readline()
        text = f.readline()
        header = f.readline()
        # Handle some messy stuff in the log
        generallog +=  header.replace(' ','').replace('(num)','')
        picturelog +=  header.replace(' ','').replace('(num)','')
        while True:
            text = f.readline()
            if not text:
                break
            splittext = text.split('\t')
            if len(splittext) < 2:
                continue
            if 'Pulse' in splittext[1] or 'Port Input' in splittext[1]:
                generallog += text
            elif 'Picture' in splittext[1]:
                picturelog += text
            elif 'Manual' in splittext:
                manuallog.append(splittext[2])

    df = pd.read_table(StringIO(generallog))
    df.Time /= 10000.
    vstim = pd.read_table(StringIO(picturelog))
    vstim.Time /= 10000.
    frametimes = np.array(df.Time[df['Code'] == 30])
    frametimes = frametimes[frametimes >= vstim.Time.iloc[0]]
    enc1times = np.array(df.Time[df['Code'] == 3])
    enc2times = np.array(df.Time[df['Code'] == 4])
    laptimes = np.array(df.Time[df['Code'] == 5])
    behaviordict = behaviorFromEncoderTicksAndPhotosensor(enc1times,
                                                          enc2times,
                                                          laptimes)
    return dict(manuallog = manuallog,
                pulseslog = df,
                frametimes=frametimes,
                pstimlog=vstim,
                **behaviordict)

from scipy.interpolate import interp1d
import scipy.signal as signal

def behaviorFromEncoderTicksAndPhotosensor(enc1,enc2,lap,
                                           verbose=False):
    ''' Computes behavior from encoder ticks and photosensor (From presentation log)
    '''
    distance = np.zeros(enc1.shape)
    for i in range(len(enc1)-1):
        if np.mod(len(enc2[(enc2 > enc1[i]) & (enc2 < enc1[i+1])]),2):
            distance[i+1] = +1;
        else:
            distance[i+1] = -1;
    if (len(distance)<2):
        print('Not enough pulses on the encoders to compute velocity.')
        return None
    position = np.cumsum(distance)
    # Normalize position to laps        
    vsrate = 1./np.min(np.diff(enc1))    
    vtime = np.arange(min(enc1),max(enc1),1./vsrate)
    fpos = interp1d(enc1,position)
    pos = fpos(vtime)
    #def normpdf(x, mu, sigma):
    #    return 1/(sigma*np.sqrt(2*np.pi))*np.exp(-1*(x-mu)**2/2*sigma**2)

    npos = pos.copy()
    time = vtime.copy()
    mlapticks = []
    if len(np.where(np.diff(lap) < 1)[0]):
        print('There are laps shorter than 1 second.. removing them.')
        lap = np.delete(lap,np.where(np.diff(lap) < 1)[0])
    for i,(s,e) in enumerate(zip(lap[:-1],lap[1:])):
        tmpidx = np.where((time>=s) & (time<e))[0]
        tmp = npos[tmpidx]
        npos[tmpidx] = (i+1.)+(tmp - tmp[0])/(tmp[-1] - tmp[0])
        mlapticks.append(tmp[-1] - tmp[0])
    mlapticks = np.array(mlapticks)
    if verbose:
        print('''    Median lap ticks: {0}
        Mean lap ticks: {1}
        Std lap ticks: {2}
        Min Max: {3},{4}
        '''.format(int(np.median(mlapticks)),
                   int(np.nanmean(mlapticks)),
                   int(np.std(mlapticks)),
                   int(np.nanmin(mlapticks)),
                   int(np.nanmax(mlapticks))))
        if np.nanmin(mlapticks)<250:
            print('\n'.join([str(int(l)) for l in mlapticks]))
    # the first lap (Assuming trigger on lap)
    tmpidx = np.where((time<lap[0]))[0]
    tmp = npos[tmpidx]
    npos[tmpidx] = tmp/tmp[-1]
    # the last lap (Assuming trigger on lap)
    tmpidx = np.where((time>lap[-1]))[0]
    tmp = npos[tmpidx]
    npos[tmpidx] = len(lap) + (tmp - tmp[0])/np.mean(mlapticks)
    distance = npos
    position = np.mod(distance,1.)
    #x = np.arange(-3*sigma*1e-3,-3*sigma*1e-3,1/vsrate)
    #kern = normpdf(np.arange(-5*sigma*1.0e-3,5*sigma*1.0e-3,1/vsrate),0,sigma*1.0e-3)
    velocity = np.diff(np.hstack([distance,distance[-1]]))/(vtime[1]-vtime[0])
    #velocity = signal.convolve(np.diff(distance)/(vtime[1]-vtime[0]),
    #                           kern/sum(kern),'same')
    return dict(behaviortime = time,
                behaviorposition = position,
                behaviorvelocity = velocity,
                laptimes = lap,
                licktimes = np.array([]))

