from .utils import structuredArrayToDict
import numpy as np


def addSessionRuns(session,sessionRuns):
    from .schemas import Session,SessionRun, Mouse, dj

    Session() & ('mouse_id = "{0}"'.format(session[0].astype(str)))
    #dje.TwoPhotonVolume()
    mouse_id = session[0].astype(str)
    assert Mouse() & ('mouse_id = "{0}"'.format(mouse_id)), "Add mouse [{0}] first.".format(mouse_id)
    try:
        Session().insert1(structuredArrayToDict(session))
    except dj.DataJointError:
        print('Session existed - {0}.'.format(session['session_name'].astype(str)))
    for run in sessionRuns:
        run = structuredArrayToDict(run)
        run['session_name'] = session['session_name'].astype(str)
        try:
            SessionRun().insert1(run)
        except dj.DataJointError:
            print('Session run existed - {0}.'.format(session['session_name'].astype(str)))


def lap_map_from_timestamps(timestamps,key,interrate = 90.):
    from .schemas import (TreadmillPositionExperiment,
                          TreadmillBelt,
                          Behavior)
    from .utils import lowpass
    from .behaviorutils import binToLaps
    nk = (TreadmillPositionExperiment()*TreadmillBelt() & key)

    belt_length = nk.fetch('belt_length')[0]

    timebehav = (Behavior() & key).fetch('behaviortime')[0]
    posbehav =  (Behavior() & key).fetch('behaviorposition')[0]
    velbehav = (Behavior() & key).fetch('behaviorvelocity')[0]
    brate = 1./np.diff(timebehav[:2])
    laps = (Behavior() & key).fetch('laptimes')[0]
    time = np.arange(laps[0],laps[-1],1./interrate)
    from scipy.interpolate import interp1d
    pos = interp1d(timebehav,posbehav)(time)*belt_length
    vel = interp1d(timebehav,lowpass(
        velbehav,
        Wfraction = 8./(brate/2.)))(time)*belt_length
    lapvec = np.vstack([laps[:-1],laps[1:]]).T
    vec = np.zeros_like(time)
    for t in timestamps:
        vec[(time>=t[0]) & (time<t[1])] = 10
    lapspace = np.arange(0,belt_length,1.)

    Xd = binToLaps(lapvec,
                   time,
                   pos,
                   np.expand_dims(vec,0),
                   velocity = vel,
                   velocityThreshold = 3,
                   lapspace=lapspace)
    return Xd.squeeze()
