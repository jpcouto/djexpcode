
def replaceField(pointer,field,value):
    ''' replaces a field in the pointer structure 
    Example: replace gender
        mice = dje.Mouse()
        mouse = mice & ('mouse_id = "JC060"')
        replaceField(mouse,'gender','F')
'''
    tmp = pointer.fetch()
    tmp[field] = value
    pointer.insert(tmp,replace = True)
