from glob import glob
from os.path import join as pjoin
import os
import numpy as np
from os.path import join as pjoin
from os import stat as fstat
from copyutils import *

def searchDataServers(datapath, extension,wildchar = '*.', verbose = False):
    from .utils import preferences
    for server in preferences['datapaths']['dataserverpaths']:
        path = pjoin(server,datapath)
        if verbose:
            print(path)
        if len(extension + wildchar):
            path = pjoin(path,wildchar + extension)
        files = np.sort(glob(path))
        if len(files):
            return files
    return []

def listSessions(afilter = '',exptype = '2P', usernames=None):
    if usernames is None:
        from .schemas import Experimenter
        users = Experimenter()
        usernames = users.fetch()['user_id'].astype(str)
    from .utils import preferences
    datapaths = preferences['datapaths']
    dataserverpaths = datapaths['dataserverpaths']
    if exptype == '2P':
        datapath = datapaths['twophotonpaths']['raw']
    elif exptype == '1P':
        datapath = datapaths['onephotonpaths']
    folders = searchDataServers(datapath,'*'+afilter+'*',wildchar = '')
    
    sdt = np.dtype([('mouse_id','S20'),
                    ('session_date','S10'),
                    ('user_id','S2'),
                    ('experiment_type','S12'),
                    ('experiment_setup','uint8'),
                    ('session_name','S64'),
                    ('session_notes','S4000')])
    sessiondata = np.empty((len(folders),),dtype = sdt)
    for i,f in enumerate(folders):
        toparse = os.path.basename(f).split('_')
        date,name = toparse[:2]
        # format date for datajoint
        date = '20{0}-{1}-{2}'.format(date[:2],date[2:4],date[4:])
        # find the username (from a list of usernames)
        ii = np.where(np.array([len(np.where(usernames == t)[0]) for t in toparse]))[0]
        assert len(ii) != 0, 'Could not parse user from folder: ' + f 
        username = usernames[usernames == toparse[ii[0]]][0]
        # find out the session number
        sessioninc = np.sum(
            (np.array(date,dtype='S10')== sessiondata[:]['session_date']) &
            (np.array(name,dtype='S20')== sessiondata[:]['mouse_id']))
        # create session list
        sessiondata[i] = np.array((name,
                                   date,
                                   username,
                                   exptype,
                                   1,
                                   os.path.basename(f)
                                   ,''),dtype = sdt)
    # Look at each run
    srdt = np.dtype([('mouse_id','S20'),
                     ('session_date','S10'),
                     ('session_name','S64'),
                     ('run_num','uint8'),
                     ('session_subname','S64'),
                     ('session_subnotes','S4000')])
    runsdata = []
    for i,sesname in enumerate(sessiondata['session_name']):
        runs = searchDataServers(pjoin(datapath,sesname.astype(str)),'*',wildchar = '')
        runs = list(filter(os.path.isdir,runs))
        nRuns = len(runs)
        runsdata.append(np.empty((nRuns,),dtype=srdt))
        mid,sdate,snum = sessiondata[['mouse_id','session_date','session_name']][i]
        
        for iRun in range(nRuns):
            if exptype == '2P' and len(glob(pjoin(runs[iRun],'*.sbx'))):
                sessiondata[i]['experiment_setup'] = 2
            comments = ''
            runsdata[-1][iRun] = np.array((mid,
                                           sdate,
                                           snum,
                                           iRun,
                                           os.path.basename(runs[iRun]),
                                           comments),dtype = srdt)
    return sessiondata,runsdata

