#!/usr/bin/env python
# Install script imaging code tools
# Joao Couto - May 2017

import os
from os.path import join as pjoin
from setuptools import setup
from setuptools.command.install import install


longdescription = ''' Datajoint analysis and tools'''
data_path = pjoin(os.path.expanduser('~'), 'djexpcode')

setup(
  name = 'djexpcode',
  version = '0.0',
  author = 'Joao Couto',
  author_email = 'jpcouto@gmail.com',
  description = (longdescription),
  long_description = longdescription,
  license = 'GPL',
  packages = ['djexpcode'],
)
